import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {first, flatMap, map, tap} from 'rxjs/operators';
import {DialogForceLoginComponent} from '../account/dialog-force-login/dialog-force-login.component';
import {MatDialog} from '@angular/material/dialog';
import {environment} from '../../environments/environment';
import {UserIntegrationService} from './integration/user-integration.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private token$: BehaviorSubject<string> = new BehaviorSubject<string>('');

    constructor(private http: HttpClient,
                private router: Router,
                private userIntegrationService: UserIntegrationService,
                public dialog: MatDialog) {
        this.initWithLocalToken();
        this.token$
            .pipe(map(token => !token ? '' : token))
            .subscribe(token => localStorage.setItem(environment.LS.token, token));
    }

    isLoggedIn(): Observable<boolean> {
        return this.token$.pipe(
            map(is => !!is)
        );
    }

    forceLoggedin(): Observable<boolean> {
        return this.isLoggedIn().pipe(
            flatMap(isLoggedIn => {
                if (!isLoggedIn) {
                    return this.openLoginDialog();
                }
                return of(true);
            }),
            first()
        );
    }

    login(email: string, password: string): Observable<boolean> {
        return this.userIntegrationService.login({email, password}).pipe(
            tap(response => this.token$.next(response)),
            map(response => !!response)
        );
    }

    getToken() {
        return this.token$.getValue();
    }

    logout(url: string = null) {
        this.token$.next(null);
        if (!!url) {
            this.router.navigateByUrl(url);
        } else {
            location.reload();
        }
    }

    register(userAuth: { email: string, password: string }) {
        this.userIntegrationService.register(userAuth).subscribe(r => {
            this.login(userAuth.email, userAuth.password).subscribe(re =>
                this.router.navigateByUrl('/account')
            );
        });
    }

    deregister(): void {
        this.userIntegrationService.deregister().subscribe(r => this.logout('/'));
    }

    private initWithLocalToken() {
        const localToken = localStorage.getItem(environment.LS.token);
        this.token$.next(localToken);
    }

    private openLoginDialog(): Observable<boolean> {
        const dialogRef = this.dialog.open(DialogForceLoginComponent);
        return dialogRef.afterClosed().pipe(flatMap((data => {
            if (!data || !data.email || !data.password) {
                return of(false);
            }
            return this.login(data.email, data.password);
        })));
    }

}
