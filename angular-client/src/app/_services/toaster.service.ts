import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Article} from '../_models/article';

@Injectable({
    providedIn: 'root'
})
export class ToasterService {

    constructor(private _snackBar: MatSnackBar) {
    }

    articleAdded(article: Article): void {
        const message = `'${article.name}' wurde in Warenkorb gelegt`;
        this.message(message);
    }

    unauthorized() {
        this._snackBar.open('Ungültige Authenisierung', 'X', {
            duration: 3000,
        });
    }

    error() {
        this._snackBar.open('Ein Fehler ist aufgetreten', 'X', {
            duration: 3000,
        });
    }

    private message(message: string): void {
        this._snackBar.open(message, 'X', {
            duration: 3000,
        });
    }
}
