import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class RouterHelperService {

    constructor(private router: Router) {
    }

    removeQueryParam(queryParamName: string) {
        let extras: any = {
            queryParams: {},
            queryParamsHandling: 'merge'
        };
        extras.queryParams[queryParamName] = null;
        this.router.navigate([], extras);
    }

    addQueryParamToUrl(url: string, queryName: string, queryValue: string | number | boolean): string {
        const querySpacer = url.indexOf('?') >= 0 ? '&' : '?';
        url += querySpacer + queryName + '=' + queryValue;
        return url;
    }

}
