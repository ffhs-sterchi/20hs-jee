import {Injectable, NgZone} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ArticleIntegrationService} from './article.integration.service';
import {Article} from '../../_models/article';

@Injectable({
    providedIn: 'root'
})
export class RecommendationIntegrationService {
    public article$: BehaviorSubject<any> = new BehaviorSubject<Article>(null);
    private readonly recommendationBaseUrl = environment.apiBaseUrl + 'recommendation/lastViewed';
    private readonly source = new EventSource(this.recommendationBaseUrl);

    constructor(private zone: NgZone,
                private articleIntegrationService: ArticleIntegrationService) {
        this.source.onmessage = event => this.nextEvent(event);
    }

    private nextEvent = (event: MessageEvent) => this.zone.run(() => {
        const data: string = event.data;
        const id = parseInt(data.split('/')[2], 10);
        if (isNaN(id)) {
            return;
        }
        this.articleIntegrationService.getArticle(id).subscribe(a => this.article$.next(a));
    })
}
