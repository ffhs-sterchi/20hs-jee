import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Article} from '../../_models/article';

@Injectable({
    providedIn: 'root'
})
export class ArticleIntegrationService {
    private readonly articleBaseUrl = environment.apiBaseUrl + 'article/';

    constructor(private httpClient: HttpClient) {
    }

    getArticle(id: number | string): Observable<Article> {
        return this.httpClient.get<Article>(this.articleBaseUrl + id).pipe(map(a => new Article(a)));
    }
}
