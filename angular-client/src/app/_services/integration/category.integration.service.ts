import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Category} from '../../_models/category';

@Injectable({
    providedIn: 'root'
})
export class CategoryIntegrationService {
    private readonly categoryBaseUrl = environment.apiBaseUrl + 'category/';

    constructor(private httpClient: HttpClient) {
    }

    getCategory(id: number | string): Observable<Category> {
        return this.httpClient.get<Category>(this.categoryBaseUrl + id).pipe(map(c => new Category(c)));
    }

    getCategories(): Observable<Category[]> {
        return this.httpClient.get<Category[]>(this.categoryBaseUrl).pipe(tap(cs => cs.forEach(c => new Category(c))));
    }
}
