import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {ToasterService} from '../toaster.service';
import {User} from '../../account/User';
import {UserAuth} from '../../account/UserAuth';

@Injectable({
    providedIn: 'root'
})
export class UserIntegrationService {
    private readonly userBaseUrl = environment.apiBaseUrl + 'user';

    constructor(private httpClient: HttpClient,
                private toaster: ToasterService) {
    }

    getUserInfo(): Observable<User> {
        return this.httpClient.get<User>(this.getUrl('/me')).pipe(map(u => new User(u)));
    }

    save(user: User): Observable<User> {
        return this.httpClient.put<User>(this.getUrl('/me'), user).pipe(
            map(u => new User(u)),
            tap(u => u, () => {
                this.toaster.error();
            })
        );
    }

    deregister(): Observable<any> {
        return this.httpClient.delete<void>(this.getUrl('/'));
    }

    login(userAuth: UserAuth): Observable<any> {
        if (!userAuth.email || !userAuth.password) {
            return of(false);
        }
        return this.httpClient.post<string>(environment.apiBaseUrl + 'user/login', userAuth, {responseType: 'text' as 'json'});
    }

    register(userAuth: UserAuth): Observable<User> {
        return this.httpClient.post<User>(environment.apiBaseUrl + 'user/register', userAuth)
            .pipe(tap(u => u, () => {
                this.toaster.error();
            }));
    }

    private getUrl(path) {
        return this.userBaseUrl + path;
    }
}
