import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {filter, map, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {Article} from '../../_models/article';
import {ToasterService} from '../toaster.service';
import {AuthService} from '../auth.service';
import {RouterHelperService} from '../router-helper.service';
import {Order} from '../../_models/order';

@Injectable({
    providedIn: 'root'
})
export class BasketIntegrationService {
    private readonly BASKET_BASEURL = environment.apiBaseUrl + 'basket/';
    private readonly ORDER_BASEURL = environment.apiBaseUrl + 'order/';
    private readonly ADD_ARTICLE_QUERY_PARAM = 'addArticle';

    private readonly basket$: BehaviorSubject<Article[]> = new BehaviorSubject<Article[]>([]);

    constructor(private httpClient: HttpClient,
                private toaster: ToasterService,
                private authService: AuthService,
                private route: ActivatedRoute,
                private routerHelper: RouterHelperService) {
        this.addArticleIfInQuery();
    }

    addArticleIfInQuery() {
        this.route.queryParams.subscribe(query => {
            const queryElement = parseInt(query[(this.ADD_ARTICLE_QUERY_PARAM)], 10);
            if (queryElement) {
                this.addArticle(queryElement);
                this.routerHelper.removeQueryParam('addArticle');
            }
        });
    }

    getBasket(): BehaviorSubject<Article[]> {
        this.httpClient.get<Article[]>(this.BASKET_BASEURL).subscribe(basket => this.basket$.next(basket));
        return this.basket$;
    }

    addArticle(articleId: number) {
        this.authService.forceLoggedin().subscribe(() => {
            this.httpClient.put<Article[]>(this.BASKET_BASEURL + articleId, null)
                .pipe(filter(basket => basket != null && basket.length >= 0))
                .subscribe(basket => {
                    this.basket$.next(basket);
                    let articles = basket.filter(a => a.id == articleId) || [];
                    this.toaster.articleAdded(articles.length > 0 ? articles[0] : (<Article> {name: ''}));
                });
        });

    }

    removeArticle(articleId: number) {
        this.httpClient.delete<Article[]>(this.BASKET_BASEURL + articleId)
            .pipe(filter(basket => basket != null && basket.length >= 0))
            .subscribe(basket => {
                this.basket$.next(basket);
            });
    }

    placeOrder(): Observable<any> {
        return this.httpClient.post(this.ORDER_BASEURL, {}).pipe(
            tap(u => u, () => {
                this.toaster.error();
            })
        );
    }


    getOrder(id: string): Observable<Order> {
        return this.getOrders().pipe(
            map(orders => orders.find(o => o.id === parseInt(id, 10)))
        );
    }

    getOrders(): Observable<Order[]> {
        return this.httpClient.get<Order[]>(this.ORDER_BASEURL);
    }
}
