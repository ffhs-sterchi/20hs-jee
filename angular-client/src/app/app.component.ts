import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from './_services/auth.service';
import {Category} from './_models/category';
import {CategoryIntegrationService} from './_services/integration/category.integration.service';

@Component({
    selector: 'ec-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    categories$: Observable<Category[]>;
    isLoggedIn = false;

    constructor(private categoryService: CategoryIntegrationService,
                private authService: AuthService) {
        this.categories$ = this.categoryService.getCategories();
        this.authService.isLoggedIn().subscribe(l => this.isLoggedIn = l);
    }
}
