import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ArticleDetailComponent} from './article/article-detail/article-detail.component';
import {CategoryComponent} from './article/category/category.component';
import {AuthGuard} from './_services/auth.guard';
import {AccountComponent} from './account/account.component';
import {RegisterComponent} from './account/register/register.component';
import {OrderConfirmationComponent} from './order/order-confirmation/order-confirmation.component';
import {BasketComponent} from './order/basket/basket.component';


const routes: Routes = [
    {
        component: HomeComponent,
        path: '',
        pathMatch: 'full'
    },
    {
        component: AccountComponent,
        path: 'account',
        canActivate: [AuthGuard]
    },
    {
        component: CategoryComponent,
        path: 'category/:id'
    },
    {
        component: ArticleDetailComponent,
        path: 'article/:id'
    },
    {
        component: OrderConfirmationComponent,
        path: 'order/:id'
    },
    {
        component: RegisterComponent,
        path: 'register'
    },
    {
        component: BasketComponent,
        path: 'cart',
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
