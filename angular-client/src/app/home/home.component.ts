import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Category} from '../_models/category';
import {CategoryIntegrationService} from '../_services/integration/category.integration.service';

@Component({
    selector: 'ec-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    categories$: Observable<Category[]>;

    constructor(private categoryService: CategoryIntegrationService) {

    }

    ngOnInit(): void {
        this.categories$ = this.categoryService.getCategories();
    }

}
