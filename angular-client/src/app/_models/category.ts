import {Article} from './article';

export class Category {
    id: number;
    name?: string;
    description?: string;
    articles?: Article[];

    constructor(c: Category) {
        Object.assign(this, c);
        this.articles = this.articles ? this.articles.map(a => new Article(a)) : [];
    }
}
