import {environment} from '../../environments/environment';

export class Article {
    id: number;
    name?: string;
    description?: string;
    price?: number;

    constructor(a: Article | any) {
        Object.assign(this, a);
    }

    private _imageurl?: string;

    get imageurl(): string {
        if (this._imageurl == null || this._imageurl === '') {
            return 'assets/30-300x200.jpg';
        }
        return (this._imageurl.startsWith('http') ? '' : environment.apiBaseUrl) + this._imageurl;
    }

    set imageurl(value: string) {
        this._imageurl = value;
    }

}
