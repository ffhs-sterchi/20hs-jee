import {Article} from './article';

export class Order {
    id: number;
    articles: Article[];
}
