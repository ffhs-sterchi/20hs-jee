import {Component, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {Order} from '../../_models/order';
import {User} from '../../account/User';
import {Article} from '../../_models/article';
import {BasketIntegrationService} from '../../_services/integration/basket.integration.service';
import {UserIntegrationService} from '../../_services/integration/user-integration.service';

@Component({
    selector: 'ec-basket',
    templateUrl: './basket.component.html',
    styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
    articles$: BehaviorSubject<Article[]>;
    public userForm = new FormGroup({
        id: new FormControl(),
        fullname: new FormControl(),
        email: new FormControl(),
        address: new FormControl(),
        zipcode: new FormControl(),
        city: new FormControl(),
    });
    needsAddress: boolean;
    addressComplete: boolean;


    constructor(private basketService: BasketIntegrationService,
                private userIntegrationService: UserIntegrationService,
                private router: Router) {
        this.articles$ = this.basketService.getBasket();
        this.userIntegrationService.getUserInfo().subscribe(user => {
            this.userForm.patchValue(user);
            this.needsAddress = !user.hasCompleteAddress();
            this.addressComplete = user.hasCompleteAddress();
        });
        this.userForm.valueChanges.subscribe(data => {
            this.addressComplete = new User(data).hasCompleteAddress();
        });
    }

    ngOnInit(): void {
    }

    removeArticle(article: Article) {
        this.basketService.removeArticle(article.id);
    }

    submitOrder() {
        const placeOrder = () => this.basketService.placeOrder().subscribe(order => this.toConfirmationPage(order));

        if (this.needsAddress) {
            this.userIntegrationService.save(this.userForm.value).subscribe(user => {
                placeOrder();
            });
        } else {
            placeOrder();
        }
    }

    toConfirmationPage(order: Order) {
        this.router.navigateByUrl('/order/' + order.id);
    }
}
