import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Order} from '../../_models/order';
import {BasketIntegrationService} from '../../_services/integration/basket.integration.service';

@Component({
    selector: 'ec-order-confirmation',
    templateUrl: './order-confirmation.component.html',
    styleUrls: ['./order-confirmation.component.scss']
})
export class OrderConfirmationComponent implements OnInit {

    order: Order;

    constructor(private route: ActivatedRoute,
                private basketService: BasketIntegrationService) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.basketService.getOrder(params.get('id')).subscribe(order => this.order = order);
        });
    }

}
