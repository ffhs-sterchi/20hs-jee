import {Component, Input, OnInit} from '@angular/core';
import {BasketIntegrationService} from '../../_services/integration/basket.integration.service';
import {Article} from '../../_models/article';

@Component({
    selector: 'ec-article-preview',
    templateUrl: './article-preview.component.html',
    styleUrls: ['./article-preview.component.scss']
})
export class ArticlePreviewComponent implements OnInit {
    @Input()
    article: Article;

    constructor(private basketService: BasketIntegrationService) {
    }

    ngOnInit(): void {
    }

    addToCart(articleId: number, $event = null) {
        this.basketService.addArticle(articleId);
        if ($event) {
            $event.preventDefault();
            $event.stopPropagation();
        }
    }

}
