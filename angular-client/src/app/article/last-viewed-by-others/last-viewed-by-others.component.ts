import {Component, OnInit} from '@angular/core';
import {filter} from 'rxjs/operators';
import {Article} from '../../_models/article';
import {RecommendationIntegrationService} from '../../_services/integration/recommendation-integration.service';

@Component({
    selector: 'ec-last-viewed-by-others',
    templateUrl: './last-viewed-by-others.component.html',
    styleUrls: ['./last-viewed-by-others.component.scss']
})
export class LastViewedByOthersComponent implements OnInit {
    articles: Article[] = [];

    constructor(private rec: RecommendationIntegrationService) {
        this.rec.article$
            .pipe(filter(a => a != null))
            .subscribe(article => this.addArticleAndFixSize(article));
    }

    ngOnInit(): void {
    }

    private addArticleAndFixSize(article) {
        this.articles.unshift(article);
        this.articles = this.articles.slice(0, 3);
    }

}
