import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {Article} from '../../_models/article';
import {BasketIntegrationService} from '../../_services/integration/basket.integration.service';
import {ArticleIntegrationService} from '../../_services/integration/article.integration.service';

@Component({
    selector: 'ec-article-detail',
    templateUrl: './article-detail.component.html',
    styleUrls: ['./article-detail.component.scss']
})
export class ArticleDetailComponent implements OnInit {
    article$: Observable<Article>;

    constructor(private route: ActivatedRoute,
                private service: ArticleIntegrationService,
                private basketService: BasketIntegrationService) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params =>
            this.article$ = this.service.getArticle(params.get('id'))
        );
    }

    addToCart(article: Article) {
        this.basketService.addArticle(article.id);
    }
}
