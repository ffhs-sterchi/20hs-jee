import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {Category} from '../../_models/category';
import {CategoryIntegrationService} from '../../_services/integration/category.integration.service';

@Component({
    selector: 'ec-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

    category$: Observable<Category> = of({
        id: 0,
        name: '',
        description: '',
        articles: []
    });

    constructor(private route: ActivatedRoute,
                private service: CategoryIntegrationService) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params =>
            this.category$ = this.service.getCategory(params.get('id'))
        );
    }
}
