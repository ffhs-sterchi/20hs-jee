export class User {
    id: string;
    email: string;
    fullname: string;
    address: string;
    zipcode: string;
    city: string;

    constructor(user: User) {
        Object.assign(this, user);
    }

    hasCompleteAddress(): boolean {
        return !!this.address && !!this.city && !!this.zipcode && !!this.fullname;
    }
}
