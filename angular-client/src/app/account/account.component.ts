import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../_services/auth.service';
import {UserIntegrationService} from '../_services/integration/user-integration.service';
import {Order} from '../_models/order';
import {BasketIntegrationService} from '../_services/integration/basket.integration.service';

@Component({
    selector: 'ec-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
    public userForm = new FormGroup({
        id: new FormControl(),
        fullname: new FormControl(),
        email: new FormControl(),
        address: new FormControl(),
        zipcode: new FormControl(),
        city: new FormControl(),
    });
    orders: Order[];

    constructor(protected readonly userService: UserIntegrationService,
                private readonly authService: AuthService,
                private basketService: BasketIntegrationService) {
    }

    ngOnInit(): void {
        this.basketService.getOrders().subscribe(orders => this.orders = orders);
        this.userService.getUserInfo().subscribe(u => {
            this.userForm.patchValue(u);
        });
    }

    logout() {
        this.authService.logout('/');
    }

    save() {
        this.userService.save(this.userForm.value).subscribe(u => this.userForm.patchValue(u));
    }

    deregister() {
        this.authService.deregister();
    }
}
