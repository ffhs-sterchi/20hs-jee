import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../../_services/auth.service';

@Component({
    selector: 'ec-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    public userForm = new FormGroup({
        email: new FormControl(),
        password: new FormControl(),
    });

    constructor(protected readonly userService: AuthService) {
    }

    ngOnInit(): void {
    }

    save() {
        this.userService.register(this.userForm.value);
    }

}
