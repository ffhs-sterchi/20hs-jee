import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'ec-dialog-force-login',
    templateUrl: './dialog-force-login.component.html',
    styleUrls: ['./dialog-force-login.component.scss']
})
export class DialogForceLoginComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data) {
        this.data = {email: '', password: ''};
    }

    ngOnInit(): void {
    }
}
