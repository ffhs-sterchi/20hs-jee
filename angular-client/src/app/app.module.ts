import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {HomeComponent} from './home/home.component';
import {MatCardModule} from '@angular/material/card';
import {ArticleDetailComponent} from './article/article-detail/article-detail.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CategoryComponent} from './article/category/category.component';
import {AccountComponent} from './account/account.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {DialogForceLoginComponent} from './account/dialog-force-login/dialog-force-login.component';
import {MatDialogModule} from '@angular/material/dialog';
import {RegisterComponent} from './account/register/register.component';
import {TokenInterceptor} from './_services/interception/token.interceptor';
import {UnauthorizedInterceptor} from './_services/interception/unauthorized.interceptor';
import {CorrelationIdInterceptor} from './_services/interception/correlation-id.interceptor';
import {ArticlePreviewComponent} from './article/article-preview/article-preview.component';
import {LastViewedByOthersComponent} from './article/last-viewed-by-others/last-viewed-by-others.component';
import {OrderConfirmationComponent} from './order/order-confirmation/order-confirmation.component';
import {BasketComponent} from './order/basket/basket.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ArticleDetailComponent,
        CategoryComponent,
        AccountComponent,
        BasketComponent,
        DialogForceLoginComponent,
        RegisterComponent,
        OrderConfirmationComponent,
        ArticlePreviewComponent,
        LastViewedByOthersComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatCardModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatDialogModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CorrelationIdInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UnauthorizedInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
