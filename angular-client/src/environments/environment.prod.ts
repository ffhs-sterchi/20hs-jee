export const environment = {
    production: true,
    apiBaseUrl: 'http://localhost:8080/',
    spaBaseUrl: 'http://localhost:4200/',
    LS: {
        token: 'JEE_ECOMM_TOKEN'
    }
};
