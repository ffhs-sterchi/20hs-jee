import {v4 as uuid} from 'uuid';
import 'cypress-localstorage-commands';

describe('HomePage', () => {
    beforeEach(() => {
        cy.visit('/');
        cy.contains('mat-card-title', 'Trinkglässer & Tassen');
        cy.contains('mat-card-title', 'Kochutensilien');
        cy.contains('Kochutensilien');
    });


    it('should navigate to category', () => {
        cy.get('mat-card[ng-reflect-router-link="/category/1"]').trigger('click');
        cy.url().should('include', '/category/1');
    });

});

describe('Category Overview', () => {
    it('should load category page', () => {
        cy.visit('/category/1');
        cy.contains('mat-card-title', 'Trinkglässer & Tassen');
        cy.contains('mat-card-title', 'Tee Tasse');
    });
    it('should navigate to article on click on preview', () => {
        cy.get('mat-card[ng-reflect-router-link="/article/1"]').trigger('click');
        cy.url().should('include', '/article/1');
    });
});

describe('Article Detail', () => {
    it('should load article page', () => {
        cy.visit('/article/1');
        cy.contains('mat-card-title', 'Tee Tasse');
        cy.contains('h3', 'CHF 12.50');
        cy.contains('button', 'in Warenkorb');
    });
    it('should open login promt on "in Warenkorb" click', () => {
        cy.get('button.add-to-cart').trigger('click');
        loginPromtIsPresent();
    });
    it('should show last viewed article on homepage', () => {
        cy.visit('/article/5');
        cy.visit('/');
        cy.contains('mat-card-title', 'Whisky-Glässer 2 Stk.');

        cy.get('mat-card[ng-reflect-router-link="/category/2"]').trigger('click');
        cy.get('mat-card[ng-reflect-router-link="/article/8"]').trigger('click');
        cy.get('#main-title').trigger('click');
        cy.contains('mat-card-title', 'Whisky-Glässer 2 Stk.');
        cy.contains('mat-card-title', 'Kochlöffel');
    });
});

describe('Authorization', () => {
    const user = {
        email: `void-${uuid()}@nvax.ch`,
        name: 'Jack Daniels',
        address: 'Wall Street 2',
        zip: '3000',
        city: 'Bern'
    };

    it('should promt for login', () => {
        cy.visit('/');
        cy.get('button[ng-reflect-router-link="/account"][aria-label="Benutzerkonto"]').trigger('click');
        loginPromtIsPresent();
    });

    it('should register user', () => {
        cy.get('button.to-register').trigger('click');

        cy.url().should('include', '/register');
        cy.contains('mat-card-title', 'Registrierung');
        cy.get('input[type="email"]').type(user.email);
        cy.get('input[type="password"]').type(user.email);
        cy.get('button.register').trigger('click');

        cy.url().should('include', '/account');
        cy.contains('mat-card-subtitle', user.email);

        cy.saveLocalStorage();
    });

    it('should set user detail', () => {
        cy.restoreLocalStorage();
        cy.url().should('include', '/account');
        cy.contains('mat-card-subtitle', user.email);

        cy.get('input[formControlName="fullname"]').type(user.name);
        cy.get('input[formControlName="address"]').type(user.address);
        cy.get('input[formControlName="zipcode"]').type(user.zip);
        cy.get('input[formControlName="city"]').type(user.city);
        cy.contains('mat-card-title', user.name);
        cy.get('button.save-user').trigger('click');

        cy.wait(100);

        cy.visit('/');
        cy.visit('/account');

        cy.get('input[formControlName="fullname"]').should('have.value', user.name);
        cy.get('input[formControlName="address"]').should('have.value', user.address);
        cy.get('input[formControlName="zipcode"]').should('have.value', user.zip);
        cy.get('input[formControlName="city"]').should('have.value', user.city);
        cy.contains('mat-card-title', user.name);
    });
});

function loginPromtIsPresent() {
    cy.contains('h2', 'Login');
    cy.contains('mat-dialog-content', 'Sie müssen zuerst eingelogt sein!');
    cy.contains('button', 'Login');
    cy.contains('button[ng-reflect-router-link="/register"].to-register', 'registrieren');
}
