import {v4 as uuid} from 'uuid';
import 'cypress-localstorage-commands';

function login() {
    const user = {
        email: `void-${uuid()}@nvax.ch`,
        fullname: 'Jack Daniels',
        address: 'Wall Street 2',
        zipcode: 3000,
        city: 'Bern'
    };

    cy.request('POST', 'http://localhost:8080/user/register', {
        email: user.email, password: user.email
    });
    cy.request('POST', 'http://localhost:8080/user/login', {
        email: user.email, password: user.email
    }).then(response => {
        cy.setLocalStorage('JEE_ECOMM_TOKEN', response.body);
        cy.saveLocalStorage();
        cy.request({
            method: 'PUT',
            url: 'http://localhost:8080/user/me',
            body: user,
            headers: {
                Authorization: `Bearer ${response.body}`
            }
        });
    });
}

describe('Order Process', () => {

    it('should login', () => {
        login();
    });

    it('should add two article to cart', () => {
        cy.restoreLocalStorage();
        cy.visit('/article/1');
        cy.contains('mat-card-title', 'Tee Tasse');
        cy.get('button.add-to-cart').trigger('click');
        cy.get('button.add-to-cart').trigger('click');
        cy.contains('snack-bar-container', '\'Tee Tasse\' wurde in Warenkorb gelegt');

        cy.get('button.cart').trigger('click');
        cy.url().should('include', '/cart');
        cy.contains('.basket-item', 'Tee Tasse');
        cy.get('.basket-item').should('have.length', 2);
    });

    it('should remove article from cart', () => {
        cy.restoreLocalStorage();
        cy.visit('/cart');
        cy.contains('.basket-item', 'Tee Tasse');
        cy.get('.basket-item').should('have.length', 2);
        cy.get('button.remove-article').first().trigger('click');

        cy.get('.basket-item').should('have.length', 1);
    });

    it('should order cart', () => {
        cy.get('button.order-cart').trigger('click');

        cy.url().should('include', '/order');
        cy.contains('mat-card-title', 'Bestellbestätigung');
        cy.contains('mat-card-subtitle', 'Bestellnummer');
        cy.contains('.basket-item', 'Tee Tasse');
        cy.contains('button', 'zur Startseite');
    });
});
