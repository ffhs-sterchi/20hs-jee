package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.dto.CategoryDto;
import ch.novarx.ffhs.ecommerce.control.CategoryService;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static java.net.URI.create;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("category")
@Tag(name = "category")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class CategoryResource {

    @Inject
    CategoryService categoryService;

    @GET
    public List<CategoryDto> getCategories() {
        return categoryService.getCategories();
    }

    @GET
    @Path("{categoryId}")
    public CategoryDto getCategory(@PathParam("categoryId") long categoryId) {
        return categoryService.getCategory(categoryId);
    }

    @POST
    public Response createCategory(CategoryDto dto) {
        CategoryDto category = categoryService.createCategory(dto);
        return Response.created(create("/category/"))
            .entity(category)
            .build();
    }

    @PUT
    @Path("{categoryId}")
    public Response updateCategory(@PathParam("categoryId") long categoryId, CategoryDto dto) {
        categoryService.updateCategory(categoryId, dto);
        return Response.noContent().build();
    }

    @POST
    @Path("{categoryId}/article")
    public Response addArticleToCategory(@PathParam("categoryId") long categoryId, List<Long> articleIds) {
        categoryService.addArticles(categoryId, articleIds);
        return Response.noContent().build();
    }

    @DELETE
    @Path("{categoryId}/article")
    public void removeArticleFromCategory(@PathParam("categoryId") long categoryId, List<Long> articleIds) {
        categoryService.removeArticles(categoryId, articleIds);
    }
}
