package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.boundary.dto.CategoryDto;
import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.ArticleRepository;
import ch.novarx.ffhs.ecommerce.entity.Category;
import ch.novarx.ffhs.ecommerce.entity.CategoryRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;

@ApplicationScoped
public class CategoryService {

    @Inject
    ArticleRepository articleRepository;

    @Inject
    CategoryRepository categoryRepository;

    public List<CategoryDto> getCategories() {
        List<Category> all = categoryRepository.listAll();
        return all.stream().map(CategoryDto::of).collect(toList());
    }

    public CategoryDto getCategory(long categoryId) {
        Optional<Category> category = categoryRepository.findByIdOptional(categoryId);
        return CategoryDto.ofWithArticles(category.orElseThrow(() -> notFound(categoryId)));
    }

    private NotFoundException notFound(long categoryId) {
        return new NotFoundException(format("Category with id '%s' not found", categoryId));
    }

    @Transactional
    public CategoryDto createCategory(CategoryDto dto) {
        Category category = new Category();
        category.setName(dto.getName());
        category.setDescription(dto.getDescription());
        categoryRepository.persist(category);
        return CategoryDto.of(category);
    }

    @Transactional
    public void updateCategory(long categoryId, CategoryDto dto) {
        Category current = categoryRepository.findById(categoryId, PESSIMISTIC_WRITE);
        current.setName(dto.getName());
        current.setDescription(dto.getDescription());
        categoryRepository.persist(current);
    }

    @Transactional
    public void addArticles(long categoryId, List<Long> articleIds) {
        Category current = categoryRepository.findById(categoryId);
        List<Article> articles = articleRepository.list("id in ?1", articleIds);
        current.getArticles().addAll(articles);
        categoryRepository.persist(current);
    }

    @Transactional
    public void removeArticles(long categoryId, List<Long> articleIds) {
        Category current = categoryRepository.findById(categoryId);
        current.setArticles(current.getArticles().stream().filter(a -> !articleIds.contains(a.getId())).collect(toList()));
        categoryRepository.persist(current);
    }
}
