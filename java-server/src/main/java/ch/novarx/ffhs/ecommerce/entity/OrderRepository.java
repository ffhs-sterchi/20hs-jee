package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@ApplicationScoped
public class OrderRepository implements PanacheRepository<Order> {

    @Transactional
    public void save(@Valid Order order) {
        persist(order);
    }

    public List<Order> listByUserId(String userId) {
        return list("userId", userId);
    }
}
