package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.base.BaseAuthRestResource;
import ch.novarx.ffhs.ecommerce.control.OrderService;
import ch.novarx.ffhs.ecommerce.entity.Order;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

import static ch.novarx.ffhs.ecommerce.control.Roles.ADMIN_ROLE;
import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("order")
@Tag(name = "order")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class OrderResource extends BaseAuthRestResource {

    @Inject
    OrderService orderService;

    @POST
    @RolesAllowed(USER_ROLE)
    public Order placeOrder() {
        return orderService.placeOrder(authUserId());
    }

    @GET
    @Path("/{userId}")
    @RolesAllowed(ADMIN_ROLE)
    public List<Order> getOrdersForUser(@PathParam("userId") String userId) {
        return orderService.getOrdersOf(userId);
    }

    @GET
    @RolesAllowed(USER_ROLE)
    public List<Order> getOrders() {
        return orderService.getOrdersOf(authUserId());
    }
}
