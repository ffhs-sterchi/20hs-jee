package ch.novarx.ffhs.ecommerce.boundary.dto;

import ch.novarx.ffhs.ecommerce.entity.Article;

public class ArticleDto {

    private Long id;
    private String name;
    private String description;
    private Float price;
    private String imageurl;
    private Integer stock;

    public static ArticleDto of(Article article) {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setId(article.getId());
        articleDto.setName(article.getName());
        articleDto.setDescription(article.getDescription());
        articleDto.setPrice(article.getPrice());
        articleDto.setImageurl(article.getImageurl());
        articleDto.setStock(article.getStock());
        return articleDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
