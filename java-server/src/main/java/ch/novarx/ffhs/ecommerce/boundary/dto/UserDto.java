package ch.novarx.ffhs.ecommerce.boundary.dto;

import ch.novarx.ffhs.ecommerce.entity.User;

import java.util.Set;

public class UserDto {

    private String id;
    private String email;
    private String fullname;
    private String address;
    private String city;
    private String zipcode;
    private Set<String> roles;

    public static UserDto of(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setFullname(user.getFullname());
        userDto.setAddress(user.getAddress());
        userDto.setCity(user.getCity());
        userDto.setZipcode(user.getZipcode());
        userDto.setRoles(user.getRolesAsSet());
        return userDto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
