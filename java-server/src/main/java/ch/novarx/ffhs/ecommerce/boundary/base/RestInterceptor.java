package ch.novarx.ffhs.ecommerce.boundary.base;

import ch.novarx.ffhs.ecommerce.control.event.Event;
import ch.novarx.ffhs.ecommerce.control.event.RestEventService;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

@Provider
public class RestInterceptor implements ContainerRequestFilter {

    private static final String CORRELATION_ID_HEADER = "correlation-id";

    @Inject
    RestEventService restEventService;

    @Override
    public void filter(ContainerRequestContext context) {

        Event event = new Event();
        event.setCorrId(getCorrelationId(context));
        event.setMethod(context.getMethod());
        event.setPath(context.getUriInfo().getPath());

        restEventService.publish(event);
    }

    private String getCorrelationId(ContainerRequestContext context) {
        String corrId = context.getHeaderString(CORRELATION_ID_HEADER);
        if (corrId == null) corrId = UUID.randomUUID().toString();
        return corrId;
    }
}
