package ch.novarx.ffhs.ecommerce.boundary.dto;

import ch.novarx.ffhs.ecommerce.entity.Category;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class CategoryDto {

    private Long id;
    private String name;
    private String description;

    @JsonInclude(Include.NON_EMPTY)
    private List<ArticleDto> articles;

    public static CategoryDto of(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        categoryDto.setDescription(category.getDescription());
        return categoryDto;
    }

    public static CategoryDto ofWithArticles(Category category) {
        CategoryDto dto = of(category);
        dto.setArticles(category.getArticles().stream().map(ArticleDto::of).collect(toList()));
        return dto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ArticleDto> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleDto> articles) {
        this.articles = articles;
    }
}
