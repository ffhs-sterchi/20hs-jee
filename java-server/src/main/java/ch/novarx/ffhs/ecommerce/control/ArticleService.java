package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.ArticleRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.function.Supplier;

import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;

@ApplicationScoped
public class ArticleService {

    @Inject
    ArticleRepository articleRepository;

    public Article getArticle(long articleId) {
        return articleRepository.findByIdOptional(articleId).orElseThrow(notFound());
    }

    private Supplier<RuntimeException> notFound() {
        return () -> {
            throw new NotFoundException("Article not found");
        };
    }

    public List<Article> getAll() {
        return articleRepository.listAll();
    }

    public void delete(long articleId) {
        articleRepository.delete(articleId);
    }

    @Transactional
    public void update(Article article) {
        Article dbArticle = articleRepository.findByIdOptional(article.getId(), PESSIMISTIC_WRITE)
            .orElseThrow(notFound());

        dbArticle.setName(article.getName());
        dbArticle.setDescription(article.getDescription());
        dbArticle.setImageurl(article.getImageurl());
        dbArticle.setPrice(article.getPrice());
        dbArticle.setStock(article.getStock());

        articleRepository.save(dbArticle);
    }

    public void create(Article article) {
        articleRepository.save(article);
    }
}
