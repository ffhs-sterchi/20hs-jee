package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.control.event.RestEventService;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.SseElementType;
import org.reactivestreams.Publisher;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.TimeUnit;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("recommendation")
@Tag(name = "recommendation")
@RequestScoped
public class RecommendationResource {

    @Inject
    RestEventService eventService;

    @GET
    @Path("lastViewed")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType(APPLICATION_JSON)
    public Publisher<String> greetingsAsStream() {
        BehaviorSubject<String> lastViewed = eventService.getLastViewedArticle();
        return getFirstImmediateRestDebounced(lastViewed)
            .toFlowable(BackpressureStrategy.LATEST)
            .distinctUntilChanged();
    }

    private Observable<String> getFirstImmediateRestDebounced(BehaviorSubject<String> lastViewed) {
        return BehaviorSubject.concat(
            lastViewed.take(1),
            lastViewed.skip(1).debounce(2, TimeUnit.SECONDS)
        );
    }
}
