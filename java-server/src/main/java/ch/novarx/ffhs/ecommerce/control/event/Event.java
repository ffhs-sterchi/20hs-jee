package ch.novarx.ffhs.ecommerce.control.event;

import javax.validation.constraints.NotNull;

public class Event {

    @NotNull
    private String corrId;
    private String path;
    private String method;

    public String getCorrId() {
        return corrId;
    }

    public void setCorrId(String corrId) {
        this.corrId = corrId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
