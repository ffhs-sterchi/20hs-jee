package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.ArticleRepository;
import ch.novarx.ffhs.ecommerce.entity.BasketArticleRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@ApplicationScoped
public class BasketService {

    @Inject
    BasketArticleRepository basketArticleRepository;

    @Inject
    ArticleRepository articleRepository;

    public List<Article> getBasket(String userId) {
        List<Long> articleIds = basketArticleRepository.getByUserId(userId)
            .stream()
            .map(article -> article.getArticleId())
            .collect(toList());

        Map<Long, Article> articles = articleRepository.getByIds(articleIds);
        return articleIds.stream().map(articles::get).collect(toList());
    }

    public void addArticle(String userId, Long articleId) {
        basketArticleRepository.addArticle(userId, articleId);
    }

    public void removeArticle(String userId, Long articleId) {
        basketArticleRepository.removeArticle(userId, articleId);
    }

    public void removeAllArticles(String userId) {
        basketArticleRepository.removeAllArticles(userId);
    }
}
