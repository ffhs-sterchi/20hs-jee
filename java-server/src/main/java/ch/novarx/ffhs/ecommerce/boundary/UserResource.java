package ch.novarx.ffhs.ecommerce.boundary;


import ch.novarx.ffhs.ecommerce.boundary.base.BaseAuthRestResource;
import ch.novarx.ffhs.ecommerce.boundary.dto.UserAuthDto;
import ch.novarx.ffhs.ecommerce.boundary.dto.UserDto;
import ch.novarx.ffhs.ecommerce.control.UserService;
import ch.novarx.ffhs.ecommerce.entity.User;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

import static ch.novarx.ffhs.ecommerce.control.Roles.ADMIN_ROLE;
import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("user")
@Tag(name = "user")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class UserResource extends BaseAuthRestResource {

    @Inject
    UserService userService;

    @POST
    @Path("register")
    public UserDto register(UserAuthDto user) {
        return UserDto.of(userService.register(user));
    }

    @POST
    @Path("login")
    public String login(UserAuthDto user) {
        return userService.getToken(user);
    }

    @GET
    @Path("me")
    @RolesAllowed({USER_ROLE, ADMIN_ROLE})
    public UserDto me() {
        return UserDto.of(userService.getUser(authUserId()));
    }

    @GET
    @RolesAllowed(ADMIN_ROLE)
    public List<UserDto> getUsers() {
        return userService.getUsers().stream().map(UserDto::of).collect(toList());
    }

    @GET
    @Path("{userId}")
    @RolesAllowed(ADMIN_ROLE)
    public UserDto getUser(@PathParam("userId") String id) {
        return UserDto.of(userService.getUser(id));
    }

    @PUT
    @RolesAllowed(ADMIN_ROLE)
    public UserDto updateUser(User userToUpdate) {
        return UserDto.of(userService.update(userToUpdate));
    }

    @PUT
    @Path("me")
    @RolesAllowed(USER_ROLE)
    public UserDto updateMe(User userToUpdate) {
        userToUpdate.setId(authUserId());
        return UserDto.of(userService.update(userToUpdate));
    }

    @DELETE
    @RolesAllowed(USER_ROLE)
    public void deleteMe() {
        userService.deleteUser(authUserId());
    }

    @DELETE
    @Path("{userId}")
    @RolesAllowed(ADMIN_ROLE)
    public void deleteUser(@PathParam("userId") String userId) {
        userService.deleteUser(userId);
    }
}
