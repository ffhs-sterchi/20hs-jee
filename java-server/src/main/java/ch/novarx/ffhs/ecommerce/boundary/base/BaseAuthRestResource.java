package ch.novarx.ffhs.ecommerce.boundary.base;

import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.inject.Inject;

public abstract class BaseAuthRestResource {

    @Inject
    JsonWebToken jwt;

    protected String authUserId() {
        return jwt.getSubject();
    }
}
