package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.Order;
import ch.novarx.ffhs.ecommerce.entity.OrderRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;

@ApplicationScoped
public class OrderService {

    @Inject
    OrderRepository orderRepository;

    @Inject
    BasketService basketService;

    public List<Order> getOrdersOf(String userId) {
        return orderRepository.listByUserId(userId);
    }

    public Order placeOrder(String userId) {
        List<Article> basketArticles = basketService.getBasket(userId);
        if (basketArticles.isEmpty()) {
            throw new BasketEmptyException();
        }

        Order order = createOrderWith(userId, basketArticles);
        orderRepository.save(order);
        basketService.removeAllArticles(userId);
        return order;
    }

    private Order createOrderWith(String userId, List<Article> basketArticles) {
        Order order = new Order();
        order.setUserId(userId);
        order.setOrderDate(LocalDate.now());
        order.setArticles(basketArticles);
        return order;
    }

    public static class BasketEmptyException extends RuntimeException {
        BasketEmptyException() {
            super("Basket is empty");
        }
    }
}
