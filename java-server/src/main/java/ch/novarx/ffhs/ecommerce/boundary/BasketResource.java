package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.base.BaseAuthRestResource;
import ch.novarx.ffhs.ecommerce.boundary.dto.ArticleDto;
import ch.novarx.ffhs.ecommerce.control.BasketService;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("basket")
@Tag(name = "basket")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class BasketResource extends BaseAuthRestResource {

    @Inject
    BasketService basketService;

    @GET
    @RolesAllowed(USER_ROLE)
    public List<ArticleDto> getBasket() {
        return getBasketArticles();
    }

    @PUT
    @Path("{articleId}")
    @RolesAllowed(USER_ROLE)
    public List<ArticleDto> addToBasket(@PathParam("articleId") Long articleId) {
        basketService.addArticle(authUserId(), articleId);
        return getBasketArticles();
    }

    @DELETE
    @Path("{articleId}")
    @RolesAllowed(USER_ROLE)
    public List<ArticleDto> removeArticle(@PathParam("articleId") Long articleId) {
        basketService.removeArticle(authUserId(), articleId);
        return getBasketArticles();
    }

    @DELETE
    @RolesAllowed(USER_ROLE)
    public List<ArticleDto> removeArticles() {
        basketService.removeAllArticles(authUserId());
        return getBasketArticles();
    }

    private List<ArticleDto> getBasketArticles() {
        return basketService.getBasket(authUserId()).stream()
            .map(ArticleDto::of)
            .collect(toList());
    }
}
