package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@ApplicationScoped
public class ArticleRepository implements PanacheRepository<Article> {

    public Map<Long, Article> getByIds(List<Long> articleIds) {
        return list("id in ?1", articleIds).stream().collect(toMap(
            a -> a.getId(),
            a -> a
        ));
    }

    @Transactional
    public void save(@Valid Article article) {
        persist(article);
    }

    @Transactional
    public void delete(long articleId) {
        delete("id", articleId);
    }
}
