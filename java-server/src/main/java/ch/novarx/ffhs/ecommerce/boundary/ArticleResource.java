package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.control.ArticleService;
import ch.novarx.ffhs.ecommerce.entity.Article;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

import static ch.novarx.ffhs.ecommerce.control.Roles.ADMIN_ROLE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("article")
@Tag(name = "article")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class ArticleResource {

    @Inject
    ArticleService articleService;

    @GET
    @Path("{articleId}")
    public Article getArticle(@PathParam("articleId") long articleId) {
        return articleService.getArticle(articleId);
    }

    @DELETE
    @Path("{articleId}")
    @RolesAllowed(ADMIN_ROLE)
    public void deleteArticle(@PathParam("articleId") long articleId) {
        articleService.delete(articleId);
    }

    @PUT
    @RolesAllowed(ADMIN_ROLE)
    public void updateArticle(@Valid Article article) {
        articleService.update(article);
    }

    @GET
    @RolesAllowed(ADMIN_ROLE)
    public List<Article> getArticles() {
        return articleService.getAll();
    }

    @POST
    @RolesAllowed(ADMIN_ROLE)
    public Response createArticle(@Valid Article article) {
        articleService.create(article);
        return Response.created(URI.create("/article/" + article.getId())).entity(article).build();
    }
}
