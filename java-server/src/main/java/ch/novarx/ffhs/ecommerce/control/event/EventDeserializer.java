package ch.novarx.ffhs.ecommerce.control.event;

import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class EventDeserializer extends ObjectMapperDeserializer<Event> {
    public EventDeserializer() {
        super(Event.class);
    }
}
