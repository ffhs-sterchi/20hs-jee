package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.boundary.dto.UserAuthDto;
import ch.novarx.ffhs.ecommerce.entity.User;
import ch.novarx.ffhs.ecommerce.entity.UserRepository;
import io.quarkus.security.UnauthorizedException;
import io.smallrye.jwt.build.Jwt;
import org.mindrot.jbcrypt.BCrypt;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Supplier;

import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static java.lang.String.format;
import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;

@ApplicationScoped
public class UserService {

    public static final String JWT_ISSUER = "ecommerce.ffhs.novarx.ch";

    @Inject
    BasketService basketService;

    @Inject
    UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.listAll();
    }

    public User getUser(String userId) {
        Optional<User> user = userRepository.findByIdOptional(userId);
        return user.orElseThrow(notFound(userId));
    }

    private Supplier<NotFoundException> notFound(String userId) {
        return () -> new NotFoundException(format("User with id '%s' not found", userId));
    }

    @Transactional
    public User update(User userToUpdate) {
        User user = userRepository.findByIdOptional(userToUpdate.getId(), PESSIMISTIC_WRITE)
            .orElseThrow(notFound(userToUpdate.getId()));

        user.setFullname(userToUpdate.getFullname());
        user.setAddress(userToUpdate.getAddress());
        user.setZipcode(userToUpdate.getZipcode());
        user.setCity(userToUpdate.getCity());

        userRepository.save(user);
        return user;
    }

    public void deleteUser(String userId) {
        basketService.removeAllArticles(userId);
        userRepository.delete("id", userId);
    }

    public User register(UserAuthDto userToCreate) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setRoles(USER_ROLE);
        user.setEmail(userToCreate.getEmail());
        user.setPassword(BCrypt.hashpw(userToCreate.getPassword(), BCrypt.gensalt()));

        userRepository.save(user);
        return user;
    }

    public String getToken(UserAuthDto dto) {
        User user = userRepository.findByEmail(dto.getEmail())
            .orElseThrow(UnauthorizedException::new);

        throwIfWrongPassword(user, dto);

        return Jwt.issuer(JWT_ISSUER)
            .subject(user.getId())
            .expiresAt(Instant.now().plus(365, ChronoUnit.DAYS))
            .groups(user.getRolesAsSet())
            .sign();
    }

    private void throwIfWrongPassword(User user, UserAuthDto dto) {
        boolean correct = BCrypt.checkpw(dto.getPassword(), user.getPassword());
        if (!correct) {
            throw new UnauthorizedException();
        }
    }
}
