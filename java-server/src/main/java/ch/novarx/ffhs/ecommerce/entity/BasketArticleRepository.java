package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;

import static java.lang.String.format;

@ApplicationScoped
public class BasketArticleRepository implements PanacheRepository<BasketArticle> {

    public List<BasketArticle> getByUserId(String userId) {
        return list("userId", userId);
    }

    @Transactional
    public void addArticle(String userId, Long articleId) {
        BasketArticle newArticle = new BasketArticle();
        newArticle.setUserId(userId);
        newArticle.setArticleId(articleId);
        persist(newArticle);
    }

    @Transactional
    public void removeArticle(String userId, Long articleId) {
        List<BasketArticle> articles = getByUserId(userId);
        BasketArticle basketArticle = articles.stream()
            .filter(ba -> ba.getArticleId().equals(articleId))
            .findFirst()
            .orElseThrow(() -> new NotFoundException(format("Basket Article '%s' not found", articleId)));
        delete(basketArticle);
    }

    @Transactional
    public void removeAllArticles(String userId) {
        delete("user_id", userId);
    }
}
