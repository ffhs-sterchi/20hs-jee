package ch.novarx.ffhs.ecommerce.control.event;

import io.reactivex.subjects.BehaviorSubject;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;

@ApplicationScoped
public class RestEventService {

    public static final String OUTGOING_NAME = "rest-event-publish";
    public static final String INCOMING_NAME = "rest-event-consume";

    @Inject
    @Channel(OUTGOING_NAME)
    Emitter<Event> outgoingChannel;

    private BehaviorSubject<String> lastViewedArticle = BehaviorSubject.createDefault("");

    public BehaviorSubject<String> getLastViewedArticle() {
        return lastViewedArticle;
    }

    @Incoming(INCOMING_NAME)
    public void consumeEvents(Event event) {
        if (event.getPath().startsWith("/article/")) {
            lastViewedArticle.onNext(event.getPath());
        }
    }

    public void publish(@Valid Event aRequest) {
        outgoingChannel.send(aRequest);
    }
}
