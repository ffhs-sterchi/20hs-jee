CREATE TABLE basket_article
(
    id         BigInt       NOT NULL PRIMARY KEY auto_increment,
    user_id    VARCHAR(100) NOT NULL,
    article_id BigInt       NOT NULL
);
