CREATE TABLE article
(
    id          BigInt       NOT NULL PRIMARY KEY auto_increment,
    name        VARCHAR(255) NOT NULL,
    description TEXT,
    imageurl    VARCHAR(255),
    price       DECIMAL(13, 4),
    stock       INT
);
