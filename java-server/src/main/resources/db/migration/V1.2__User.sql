CREATE TABLE user
(
    id       VARCHAR(100) NOT NULL PRIMARY KEY,
    roles    VARCHAR(255) NOT NULL,
    email    VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    fullname VARCHAR(100),
    address  VARCHAR(100),
    zipcode  VARCHAR(10),
    city     VARCHAR(100)
);
