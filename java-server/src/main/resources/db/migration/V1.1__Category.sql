CREATE TABLE category
(
    id          BigInt       NOT NULL PRIMARY KEY auto_increment,
    name        VARCHAR(255) NOT NULL,
    description TEXT
);
CREATE TABLE category_article
(
    category_id BigInt NOT NULL,
    article_id  BigInt NOT NULL
);
