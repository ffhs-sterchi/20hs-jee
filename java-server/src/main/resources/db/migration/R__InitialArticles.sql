DELETE
FROM category_article;
DELETE
FROM category;
DELETE
FROM article;

INSERT INTO category(id, name, description)
VALUES (1, 'Trinkglässer & Tassen',
        'Buffalo kevin ham hock burgdoggen, ribeye ground round turkey andouille short ribs. Buffalo strip steak bacon capicola jerky ribeye ball tip. Beef frankfurter shoulder cow ribeye venison prosciutto pork chop pastrami. Sausage corned beef tail doner, strip steak pork belly pork chop landjaeger cupim bresaola pig burgdoggen rump capicola.');

INSERT INTO article(id, name, price, imageurl, description)
VALUES (1, 'Tee Tasse', 12.5, '/img/article/tee-tasse.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (2, 'Kaffee Tasse', 11.5, '/img/article/kaffee-tasse.jpg',
        'Cow leberkas ribeye ball tip. Tri-tip t-bone fatback shoulder, bacon landjaeger porchetta strip steak jowl frankfurter picanha cupim burgdoggen.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (3, 'Bierstiefel', 19.0, '/img/article/bierstiefel.jpg',
        'Cow leberkas ribeye ball tip. Tri-tip t-bone fatback shoulder, bacon landjaeger porchetta strip steak jowl frankfurter picanha cupim burgdoggen.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (4, 'Weinglässer 6 Stk.', 35.55, '/img/article/weinglas.webp',
        'Cow leberkas ribeye ball tip. Tri-tip t-bone fatback shoulder, bacon landjaeger porchetta strip steak jowl frankfurter picanha cupim burgdoggen.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (5, 'Whisky-Glässer 2 Stk.', 39.90, '/img/article/whisky-glas.jpg',
        'Cow leberkas ribeye ball tip. Tri-tip t-bone fatback shoulder, bacon landjaeger porchetta strip steak jowl frankfurter picanha cupim burgdoggen.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (6, 'Whisky-Glässer 4 Stk.', 44.20, '/img/article/whisky-glas-2.jpg',
        'Cow leberkas ribeye ball tip. Tri-tip t-bone fatback shoulder, bacon landjaeger porchetta strip steak jowl frankfurter picanha cupim burgdoggen.');

INSERT INTO category_article(category_id, article_id)
VALUES (1, 1);
INSERT INTO category_article(category_id, article_id)
VALUES (1, 2);
INSERT INTO category_article(category_id, article_id)
VALUES (1, 3);
INSERT INTO category_article(category_id, article_id)
VALUES (1, 4);
INSERT INTO category_article(category_id, article_id)
VALUES (1, 5);
INSERT INTO category_article(category_id, article_id)
VALUES (1, 6);

INSERT INTO category(id, name, description)
VALUES (2, 'Kochutensilien',
        'Chuck capicola fatback kielbasa swine tail. Pork loin corned beef strip steak tenderloin sirloin, chislic prosciutto boudin pork beef ribs doner tri-tip rump. Turducken kevin biltong pastrami tenderloin pancetta.');

INSERT INTO article(id, name, price, imageurl, description)
VALUES (7, 'Kochlöffel holz premium', 8.5, '/img/article/kochloeffel-1.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (8, 'Kochlöffel', 6.7, '/img/article/kochloeffel-2.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (9, 'Kochlöffel holz mit Loch', 5.5, '/img/article/kochloeffel-3.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (10, 'Kochlöffel holz', 5.5, '/img/article/kochloeffel-4.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (11, 'Kochlöffel holz', 5.5, '/img/article/kochloeffel-4.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (12, 'Kasserolle kupfer', 39.8, '/img/article/kasserolle-1.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (13, 'Kasserolle mit Deckel', 22.75, '/img/article/kasserolle-2.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');
INSERT INTO article(id, name, price, imageurl, description)
VALUES (14, 'Pratpfanne', 22.75, '/img/article/bratpfanne.jpg',
        'Bacon ipsum dolor amet salami tenderloin ribeye pancetta, ball tip pastrami tongue meatloaf biltong shoulder. Meatball tongue beef ribs chuck tail doner tri-tip.');

INSERT INTO category_article(category_id, article_id)
VALUES (2, 7);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 8);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 9);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 10);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 11);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 12);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 13);
INSERT INTO category_article(category_id, article_id)
VALUES (2, 14);

INSERT INTO user(id, email, password, roles)
VALUES ('eb4123a3-b722-4798-9af5-8957f823657a', 'info@novarx.ch',
        '$2a$10$LgnxXHTDBMdcaq3IbetNdODIGv/K6XD79sG5k2VHQz32NTxGlcn52', 'admin,user');

INSERT INTO basket_article(id, user_id, article_id)
VALUES (1, 'eb4123a3-b722-4798-9af5-8957f823657a', 1)
