CREATE TABLE orders
(
    id        BigInt       NOT NULL PRIMARY KEY auto_increment,
    userId    VARCHAR(255) NOT NULL,
    orderDate DATE         NOT NULL
);
CREATE TABLE order_article
(
    id         BigInt NOT NULL PRIMARY KEY auto_increment,
    order_id   BigInt NOT NULL,
    article_id BigInt NOT NULL
);
