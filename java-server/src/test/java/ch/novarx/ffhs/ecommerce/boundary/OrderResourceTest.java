package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.base.BaseRestResourceTest;
import ch.novarx.ffhs.ecommerce.control.OrderService;
import ch.novarx.ffhs.ecommerce.control.Roles;
import ch.novarx.ffhs.ecommerce.entity.Order;
import ch.novarx.ffhs.ecommerce.entity.TestUtil;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class OrderResourceTest extends BaseRestResourceTest {

    @Inject
    OrderResource sut;

    @InjectMock
    OrderService orderService;

    @Test
    @TestSecurity(user = ADMIN_ID, roles = Roles.ADMIN_ROLE)
    void should_get_orders_for_user() {
        Order order = TestUtil.aOrder();
        when(orderService.getOrdersOf(USER_ID)).thenReturn(Collections.singletonList(order));

        List<Order> response = sut.getOrdersForUser(USER_ID);

        assertThat(response).containsOnly(order);
        verify(orderService).getOrdersOf(USER_ID);
    }

    @Test
    @TestSecurity(user = USER_ID, roles = Roles.USER_ROLE)
    void should_get_orders_for_current_user() {
        String userId = stubValidUser().getId();
        Order order = TestUtil.aOrder();
        when(orderService.getOrdersOf(userId)).thenReturn(Collections.singletonList(order));

        List<Order> response = sut.getOrders();

        assertThat(response).containsOnly(order);
        verify(orderService).getOrdersOf(userId);
    }

    @Test
    @TestSecurity(user = USER_ID, roles = Roles.USER_ROLE)
    void should_place_order() {
        Order order = TestUtil.aOrder();
        String userId = stubValidUser().getId();
        when(orderService.placeOrder(userId)).thenReturn(order);

        Order placedOrder = sut.placeOrder();

        assertThat(placedOrder).isEqualTo(order);

        verify(orderService).placeOrder(userId);
    }

    @Test
    void should_be_authorized() {
        assertUnauthorized(
            () -> sut.placeOrder(),
            () -> sut.getOrders()
        );
    }

    @Test
    @TestSecurity(user = USER_ID, roles = Roles.USER_ROLE)
    void should_be_forbidden() {
        assertForbidden(
            () -> sut.getOrdersForUser(USER_ID)
        );
    }
}
