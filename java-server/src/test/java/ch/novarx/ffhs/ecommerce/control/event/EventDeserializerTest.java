package ch.novarx.ffhs.ecommerce.control.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class EventDeserializerTest {
    EventDeserializer sut = new EventDeserializer();

    @Test
    void should_deserialize() throws JsonProcessingException {
        Event event = aEvent();
        String serialized = new ObjectMapper().writeValueAsString(event);
        Event deserialized = sut.deserialize("a-topic", serialized.getBytes());

        assertThat(deserialized).usingRecursiveComparison().isEqualTo(event);
    }

    private Event aEvent() {
        Event event = new Event();
        event.setCorrId(UUID.randomUUID().toString());
        event.setMethod("DELETE");
        event.setPath("/article");
        return event;
    }
}
