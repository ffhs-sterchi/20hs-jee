package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.control.OrderService.BasketEmptyException;
import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.Order;
import ch.novarx.ffhs.ecommerce.entity.OrderRepository;
import ch.novarx.ffhs.ecommerce.entity.TestUtil;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class OrderServiceTest {
    @Inject
    OrderService sut;

    @InjectMock
    BasketService basketService;

    @InjectMock
    OrderRepository orderRepository;

    ArgumentCaptor<Order> orderCaptor = ArgumentCaptor.forClass(Order.class);

    @Test
    void should_get_all_orders_of_a_user() {
        String userId = "a-user-id";
        Order order = new Order();
        when(orderRepository.listByUserId(userId)).thenReturn(singletonList(order));

        List<Order> orders = sut.getOrdersOf(userId);

        assertThat(orders).containsOnly(order);
        verify(orderRepository).listByUserId(userId);
    }

    @Test
    void should_persist_basket_to_order() {
        String userId = "a-user-id";
        List<Article> articles = TestUtil.someBasketArticles();
        when(basketService.getBasket(userId)).thenReturn(articles);

        Order returnedOrder = sut.placeOrder(userId);

        verify(basketService).getBasket(userId);
        verify(orderRepository).save(orderCaptor.capture());

        Order order = orderCaptor.getValue();
        assertThat(returnedOrder).isEqualTo(order);
        assertThat(order.getUserId()).isEqualTo(userId);
        assertThat(order.getOrderDate()).isEqualTo(LocalDate.now());
        assertThat(order.getArticles()).hasSameSizeAs(articles);

        verify(basketService).removeAllArticles(userId);
    }

    @Test
    void should_throw_when_basket_is_empty() {
        String userId = "a-user-id";
        when(basketService.getBasket(userId)).thenReturn(emptyList());

        BasketEmptyException exception = assertThrows(BasketEmptyException.class, () -> sut.placeOrder(userId));
        assertThat(exception.getMessage()).isEqualTo("Basket is empty");
    }
}
