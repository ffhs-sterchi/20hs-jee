package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.dto.CategoryDto;
import ch.novarx.ffhs.ecommerce.control.CategoryService;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class CategoryResourceTest {

    @Inject
    CategoryResource sut;

    @InjectMock
    CategoryService categoryService;

    @Test
    void should_get_categories_from_category_service() {
        List<CategoryDto> list = emptyList();
        when(categoryService.getCategories()).thenReturn(list);

        List<CategoryDto> articles = sut.getCategories();

        assertThat(articles).isEqualTo(list);
        verify(categoryService).getCategories();
    }

    @Test
    void should_get_category_from_category_service() {
        long id = 55L;
        CategoryDto category = new CategoryDto();

        when(categoryService.getCategory(id)).thenReturn(category);

        CategoryDto response = sut.getCategory(id);

        assertThat(response).isEqualTo(category);
        verify(categoryService).getCategory(id);
    }

    @Test
    void should_create_category_with_category_service() {
        CategoryDto dto = new CategoryDto();

        when(categoryService.createCategory(dto)).thenReturn(dto);

        CategoryDto response = (CategoryDto) sut.createCategory(dto).getEntity();

        assertThat(response).isEqualTo(dto);
        verify(categoryService).createCategory(dto);
    }

    @Test
    void should_update_category_with_category_service() {
        CategoryDto dto = new CategoryDto();
        dto.setId(55L);

        sut.updateCategory(dto.getId(), dto);

        verify(categoryService).updateCategory(dto.getId(), dto);
    }

    @Test
    void should_add_article_to_category_with_category_service() {
        List<Long> articleIds = singletonList(2L);
        long categoryId = 1L;
        sut.addArticleToCategory(categoryId, articleIds);

        verify(categoryService).addArticles(categoryId, articleIds);
    }

    @Test
    void should_remove_article_from_category_with_category_service() {
        List<Long> articleIds = singletonList(2L);
        long categoryId = 1L;

        sut.removeArticleFromCategory(categoryId, articleIds);

        verify(categoryService).removeArticles(categoryId, articleIds);
    }
}
