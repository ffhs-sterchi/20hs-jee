package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.base.BaseRestResourceTest;
import ch.novarx.ffhs.ecommerce.boundary.dto.UserAuthDto;
import ch.novarx.ffhs.ecommerce.boundary.dto.UserDto;
import ch.novarx.ffhs.ecommerce.control.Roles;
import ch.novarx.ffhs.ecommerce.control.UserService;
import ch.novarx.ffhs.ecommerce.entity.TestUtil;
import ch.novarx.ffhs.ecommerce.entity.User;
import io.quarkus.security.ForbiddenException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@QuarkusTest
class UserResourceTest extends BaseRestResourceTest {

    @InjectMock
    UserService userService;

    @Inject
    UserResource sut;

    @Test
    @TestSecurity(authorizationEnabled = false)
    void should_get_user_info() {
        User mockUser = stubValidUser();
        when(userService.getUser(mockUser.getId())).thenReturn(mockUser);

        UserDto me = sut.me();

        assertThat(me.getId()).isEqualTo(mockUser.getId());
        assertThat(me.getEmail()).isEqualTo(mockUser.getEmail());
        assertThat(me.getFullname()).isEqualTo(mockUser.getFullname());

        verify(userService).getUser(mockUser.getId());
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_get_all_users() {
        User mockUser = stubValidUser();
        when(userService.getUsers()).thenReturn(List.of(mockUser));

        List<UserDto> users = sut.getUsers();

        assertThat(users.get(0).getEmail()).isEqualTo(mockUser.getEmail());

        verify(userService).getUsers();
    }

    @Test
    void should_register_user() {
        UserAuthDto user = new UserAuthDto();
        User userEntity = new User();
        when(userService.register(user)).thenReturn(userEntity);

        UserDto registered = sut.register(user);

        assertThat(registered).usingRecursiveComparison().isEqualTo(UserDto.of(userEntity));
        verify(userService).register(user);
    }

    @Test
    void should_login_user() {
        UserAuthDto dto = new UserAuthDto();

        when(userService.getToken(dto)).thenReturn("a-token");

        String token = sut.login(dto);

        assertThat(token).isEqualTo("a-token");
        verify(userService).getToken(dto);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_not_get_all_users_when_no_admin() {
        assertThrows(ForbiddenException.class, () -> sut.getUsers());
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_get_one_user() {
        User mockUser = stubValidUser();
        when(userService.getUser(mockUser.getId())).thenReturn(mockUser);

        UserDto userDto = sut.getUser(mockUser.getId());

        assertThat(userDto.getId()).isEqualTo(mockUser.getId());
        assertThat(userDto.getEmail()).isEqualTo(mockUser.getEmail());
        assertThat(userDto.getFullname()).isEqualTo(mockUser.getFullname());
        assertThat(userDto.getAddress()).isEqualTo(mockUser.getAddress());
        assertThat(userDto.getCity()).isEqualTo(mockUser.getCity());
        assertThat(userDto.getZipcode()).isEqualTo(mockUser.getZipcode());
        assertThat(userDto.getRoles()).contains(Roles.USER_ROLE);

        verify(userService).getUser(mockUser.getId());
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_not_get_one_user_when_no_admin() {
        assertThrows(ForbiddenException.class, () -> sut.getUser(""));
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_update_me() {
        User mockUser = stubValidUser();
        when(jwt.getSubject()).thenReturn(mockUser.getId());
        when(userService.update(mockUser)).thenReturn(mockUser);

        UserDto user = sut.updateMe(mockUser);

        assertThat(user).usingRecursiveComparison().isEqualTo(UserDto.of(mockUser));
        verify(userService).update(mockUser);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_update_one_user_as_admin() {
        User mockUser = stubValidUser();
        when(jwt.getSubject()).thenReturn("an-admin-id");
        when(userService.update(mockUser)).thenReturn(mockUser);

        UserDto user = sut.updateUser(mockUser);

        assertThat(user).usingRecursiveComparison().isEqualTo(UserDto.of(mockUser));
        verify(userService).update(mockUser);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_delete_me() {
        String userId = "lorem-ipsum";
        when(jwt.getSubject()).thenReturn(userId);

        sut.deleteMe();

        verify(jwt).getSubject();
        verify(userService).deleteUser(userId);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_delete_a_user() {
        String userId = "lorem-ipsum";

        sut.deleteUser(userId);

        verify(userService).deleteUser(userId);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_check_forbidden() {
        given().when().delete("/user/33").then().statusCode(403);
        given().when().contentType(APPLICATION_JSON).body(TestUtil.aUser()).put("/user").then().statusCode(403);
        verifyNoInteractions(userService);
    }

    @Test
    void should_check_unauthorized() {
        given().when().get("/user/me").then().statusCode(401);
        given().when().contentType(APPLICATION_JSON).body(TestUtil.aUser()).put("/user/me").then().statusCode(401);
        verifyNoInteractions(userService);
    }
}
