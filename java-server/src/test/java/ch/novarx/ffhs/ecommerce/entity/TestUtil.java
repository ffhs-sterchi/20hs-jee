package ch.novarx.ffhs.ecommerce.entity;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static java.util.Collections.singletonList;

public class TestUtil {
    public static Article aArticle() {
        Article article = new Article();
        article.setId(2L);
        article.setName("aArticle");
        article.setDescription("aDescrtiption");
        return article;
    }

    public static List<Article> someBasketArticles() {
        return List.of(new Article(), new Article());
    }

    public static Order aOrder() {
        Order order = new Order();
        order.setUserId("a-user-id");
        order.setOrderDate(LocalDate.of(2020, 1, 2));
        order.setArticles(singletonList(aArticle()));
        return order;
    }

    public static User aUser() {
        User user = new User();
        user.setId("lorem-ipsum");
        user.setEmail(UUID.randomUUID() + "@example.org");
        user.setPassword("a-password*");
        user.setFullname("Hans Wurst");
        user.setAddress("Street 1");
        user.setCity("Bern");
        user.setZipcode("3000");
        user.setRoles(USER_ROLE);
        return user;
    }
}
