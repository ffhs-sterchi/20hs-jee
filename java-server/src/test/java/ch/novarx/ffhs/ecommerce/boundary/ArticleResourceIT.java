package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.dto.ArticleDto;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

@QuarkusTest
@Tag("integration")
class ArticleResourceIT {

    @Test
    void should_get_404_for_not_found_article() {
        given().when()
            .get("article/9999")
            .then()
            .log().body()
            .statusCode(404);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_return_articles() {
        given().when()
            .get("article")
            .then()
            .log().body()
            .statusCode(200)
            .body("[0].name", isA(String.class))
            .body("[1].name", isA(String.class));
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_create_and_update_an_article() {
        ArticleDto createdArticle = createArticle();
        createdArticle.setDescription("Bacon Ipsum dolor sit amet.");
        createdArticle.setImageurl("http://example.com/");

        given().when()
            .contentType(APPLICATION_JSON)
            .body(createdArticle)
            .put("article")
            .then()
            .log().body()
            .statusCode(204);

        ArticleDto response = readArticle(createdArticle.getId());
        assertThat(response).isEqualToComparingFieldByField(createdArticle);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_create_and_delete_an_article() {
        ArticleDto createdArticle = createArticle();

        given().when()
            .delete("article/" + createdArticle.getId())
            .then()
            .log().body()
            .statusCode(204);

        given().when()
            .get("article/" + createdArticle.getId())
            .then()
            .statusCode(404);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_create_and_get_article() {
        ArticleDto createdArticle = createArticle();

        given().when()
            .get("article/" + createdArticle.getId())
            .then()
            .statusCode(200)
            .body("id", is(createdArticle.getId().intValue()))
            .body("name", is(createdArticle.getName()))
            .body("description", is(createdArticle.getDescription()))
            .body("price", equalTo((createdArticle.getPrice())))
            .body("stock", equalTo((createdArticle.getStock())))
            .extract().body().as(ArticleDto.class);

    }

    private ArticleDto readArticle(Long id) {
        return given().when()
            .get("article/" + id)
            .then()
            .log().body()
            .extract().body().as(ArticleDto.class);
    }

    private ArticleDto createArticle() {
        ArticleDto dto = new ArticleDto();
        dto.setName("lorem");
        dto.setDescription("ipsum");
        dto.setPrice(22F);
        dto.setImageurl("/an/image.jpg");
        dto.setStock(66);

        return given().when()
            .body(dto)
            .contentType(APPLICATION_JSON)
            .post("article")
            .then()
            .statusCode(201)
            .body("id", greaterThan(0))
            .body("name", is(dto.getName()))
            .body("description", is(dto.getDescription()))
            .body("price", equalTo(dto.getPrice()))
            .body("imageurl", equalTo(dto.getImageurl()))
            .extract().body().as(ArticleDto.class);
    }
}
