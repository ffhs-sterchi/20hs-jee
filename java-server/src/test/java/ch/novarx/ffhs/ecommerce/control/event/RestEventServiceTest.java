package ch.novarx.ffhs.ecommerce.control.event;

import ch.novarx.ffhs.ecommerce.control.KafkaTestResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.reactivex.BackpressureStrategy;
import io.reactivex.subjects.BehaviorSubject;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import java.util.List;

import static ch.novarx.ffhs.ecommerce.control.event.RestEventService.OUTGOING_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@QuarkusTest
@QuarkusTestResource(KafkaTestResource.class)
class RestEventServiceTest {
    @Inject
    @Any
    InMemoryConnector connector;

    @Inject
    RestEventService sut;

    @BeforeEach
    void clearQueue() {
        connector.sink(OUTGOING_NAME).clear();
    }

    @Test
    void should_publish_aRequest() {
        Event aRequest = new Event();
        aRequest.setCorrId("1-2-3");

        sut.publish(aRequest);

        List<? extends Message<Object>> messages = getOutgoingMessages();
        assertThat(messages).hasSize(1);
        assertThat(messages.get(0).getPayload()).isEqualTo(aRequest);
    }

    @Test
    void should_only_publish_valid_requests() {
        Event aRequest = new Event();

        assertThrows(ConstraintViolationException.class, () -> sut.publish(aRequest));
        assertThat(getOutgoingMessages()).isEmpty();
    }

    @Test
    void should_publish_consumed_event_when_article() {
        Event event = new Event();
        event.setPath("/article/55");
        sut.consumeEvents(event);

        BehaviorSubject<String> lastViewedArticle = sut.getLastViewedArticle();

        StepVerifier.create(lastViewedArticle.toFlowable(BackpressureStrategy.BUFFER))
            .expectNext("/article/55")
            .thenCancel()
            .verify();
    }

    @Test
    void should_not_publish_consumed_event_when_no_article() {
        Event event = new Event();
        event.setPath("/category");
        sut.consumeEvents(event);

        BehaviorSubject<String> lastViewedArticle = sut.getLastViewedArticle();

        StepVerifier.create(lastViewedArticle.toFlowable(BackpressureStrategy.BUFFER))
            .expectNext("")
            .thenCancel()
            .verify();
    }

    private List<? extends Message<Object>> getOutgoingMessages() {
        return connector.sink(OUTGOING_NAME).received();
    }
}
