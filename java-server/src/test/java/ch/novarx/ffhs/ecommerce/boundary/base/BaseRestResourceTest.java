package ch.novarx.ffhs.ecommerce.boundary.base;

import ch.novarx.ffhs.ecommerce.entity.TestUtil;
import ch.novarx.ffhs.ecommerce.entity.User;
import io.quarkus.security.ForbiddenException;
import io.quarkus.security.UnauthorizedException;
import io.quarkus.test.junit.mockito.InjectMock;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

public abstract class BaseRestResourceTest {

    protected static final String ADMIN_ID = "a-admin-id";
    protected static final String USER_ID = "a-user-id";

    @InjectMock
    protected JsonWebToken jwt;

    protected void assertForbidden(Executable... endpointCalls) {
        for (Executable endpointCall : endpointCalls) {
            assertThrows(ForbiddenException.class, endpointCall);
        }
    }

    protected void assertUnauthorized(Executable... endpointCalls) {
        for (Executable endpointCall : endpointCalls) {
            assertThrows(UnauthorizedException.class, endpointCall);
        }
    }

    protected User stubValidUser() {
        User user = TestUtil.aUser();
        stubValidUser(user.getId());
        return user;
    }

    protected void stubValidUser(String userId) {
        when(jwt.getSubject()).thenReturn(userId);
    }
}
