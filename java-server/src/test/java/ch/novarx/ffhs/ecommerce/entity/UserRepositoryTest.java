package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

@QuarkusTest
class UserRepositoryTest {
    @InjectSpy
    UserRepository sut;

    @Test
    void should_create_user() {
        User user = TestUtil.aUser();
        sut.save(user);
        verify(sut).persist(user);
    }

    @Test
    void should_not_create_invalid_user() {
        User user1 = TestUtil.aUser();
        user1.setId(UUID.randomUUID().toString());
        user1.setEmail("another@ipsum.org");
        sut.save(user1);
        verify(sut).persist(user1);

        User user2 = TestUtil.aUser();
        user2.setId(user1.getId());
        assertThrows(Exception.class, () -> sut.save(user2));
        verify(sut).persist(user2);

        User user3 = TestUtil.aUser();
        user1.setEmail("another@ipsum.org");
        assertThrows(Exception.class, () -> sut.save(user3));
        verify(sut).persist(user3);
    }

    @Test
    void should_get_by_email() {
        String email = "info@novarx.ch";
        User user = sut.findByEmail(email).orElseThrow();
        assertThat(user.getEmail()).isEqualTo(email);

        assertThat(sut.findByEmail("NOT_AN_EXISTING_MAIL")).isEmpty();
    }
}
