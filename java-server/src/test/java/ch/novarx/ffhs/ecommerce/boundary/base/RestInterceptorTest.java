package ch.novarx.ffhs.ecommerce.boundary.base;

import ch.novarx.ffhs.ecommerce.control.event.Event;
import ch.novarx.ffhs.ecommerce.control.event.RestEventService;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@QuarkusTest
class RestInterceptorTest {

    private static ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);

    @InjectMock
    RestEventService restEventService;

    @Inject
    RestInterceptor sut;

    @Test
    void should_publish_event() {
        ContainerRequestContext context = mockRequest();

        sut.filter(context);
        verify(restEventService).publish(captor.capture());

        Event publishedEvent = captor.getValue();
        assertThat(publishedEvent.getCorrId()).isEqualTo(context.getHeaderString("correlation-id"));
        assertThat(publishedEvent.getMethod()).isEqualTo(context.getMethod());
        assertThat(publishedEvent.getPath()).isEqualTo(context.getUriInfo().getPath());
    }

    @Test
    void should_set_random_uuid_if_not_in_request() {
        ContainerRequestContext context = mockRequest();
        when(context.getHeaderString("correlation-id")).thenReturn(null);

        sut.filter(context);
        verify(restEventService).publish(captor.capture());

        Event publishedEvent = captor.getValue();
        assertThat(publishedEvent.getCorrId()).isNotEmpty();
        assertThat(UUID.fromString(publishedEvent.getCorrId())).isNotNull();
    }

    private ContainerRequestContext mockRequest() {
        UriInfo uri = mock(UriInfo.class);
        when(uri.getPath()).thenReturn("/lorem/ipsum");
        ContainerRequestContext context = mock(ContainerRequestContext.class);
        when(context.getHeaderString("correlation-id")).thenReturn("a-1-b-2");
        when(context.getUriInfo()).thenReturn(uri);
        when(context.getMethod()).thenReturn("GET");
        return context;
    }
}
