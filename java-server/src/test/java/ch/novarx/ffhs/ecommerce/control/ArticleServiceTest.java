package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.ArticleRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import static ch.novarx.ffhs.ecommerce.entity.TestUtil.aArticle;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class ArticleServiceTest {

    @Inject
    ArticleService sut;

    @InjectMock
    ArticleRepository articleRepository;

    @Test
    void should_get_all_articles() {
        ArrayList<Article> list = new ArrayList<>();
        when(articleRepository.listAll()).thenReturn(list);

        List<Article> all = sut.getAll();

        assertThat(all).isEqualTo(list);
        verify(articleRepository).listAll();
    }

    @Test
    void should_get_one_article() {
        Article article = new Article();
        when(articleRepository.findByIdOptional(1L)).thenReturn(of(article));

        Article one = sut.getArticle(1L);

        assertThat(one).isEqualTo(article);
        verify(articleRepository).findByIdOptional(1L);
    }

    @Test
    void should_throw_when_article_not_found() {
        when(articleRepository.findByIdOptional(99L)).thenReturn(empty());

        NotFoundException exception = assertThrows(NotFoundException.class, () -> sut.getArticle(99L));

        assertThat(exception.getMessage()).isEqualTo("Article not found");
        verify(articleRepository).findByIdOptional(99L);
    }

    @Test
    void should_create_article() {
        Article article = new Article();
        sut.create(article);
        verify(articleRepository).save(article);
    }

    @Test
    void should_delete_article() {
        sut.delete(1L);
        verify(articleRepository).delete(1L);
    }

    @Test
    void should_update_article() {
        Article articleToUpdate = aArticle();
        articleToUpdate.setName("aNewName");
        articleToUpdate.setStock(99);
        articleToUpdate.setPrice(11.33F);

        Article articleFromDb = aArticle();
        when(articleRepository.findByIdOptional(articleToUpdate.getId(), PESSIMISTIC_WRITE)).thenReturn(of(articleFromDb));

        sut.update(articleToUpdate);

        assertThat(articleFromDb.getId()).isEqualTo(articleToUpdate.getId());
        assertThat(articleFromDb.getName()).isEqualTo(articleToUpdate.getName());
        assertThat(articleFromDb.getDescription()).isEqualTo(articleToUpdate.getDescription());
        assertThat(articleFromDb.getImageurl()).isEqualTo(articleToUpdate.getImageurl());
        assertThat(articleFromDb.getPrice()).isEqualTo(articleToUpdate.getPrice());
        assertThat(articleFromDb.getStock()).isEqualTo(articleToUpdate.getStock());
        verify(articleRepository).save(articleFromDb);
    }

    @Test
    void should_throw_when_article_to_update_not_found() {
        Article article = aArticle();
        when(articleRepository.findByIdOptional(article.getId(), PESSIMISTIC_WRITE)).thenReturn(empty());
        assertThrows(NotFoundException.class, () -> sut.update(article));
    }
}
