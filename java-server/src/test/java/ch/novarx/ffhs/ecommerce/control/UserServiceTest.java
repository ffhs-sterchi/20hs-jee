package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.boundary.dto.UserAuthDto;
import ch.novarx.ffhs.ecommerce.entity.User;
import ch.novarx.ffhs.ecommerce.entity.UserRepository;
import io.quarkus.security.UnauthorizedException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import io.smallrye.jwt.auth.principal.JWTCallerPrincipal;
import org.jose4j.jwt.JwtClaims;
import org.junit.jupiter.api.Test;
import org.mindrot.jbcrypt.BCrypt;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ch.novarx.ffhs.ecommerce.control.Roles.ADMIN_ROLE;
import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static java.util.Collections.singletonList;
import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@QuarkusTest
class UserServiceTest {

    @InjectMock
    BasketService basketService;

    @Inject
    UserService sut;

    @InjectMock
    UserRepository userRepository;

    private ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

    @Test
    void should_get_all_users() {
        User mockUser = getMockUser();
        when(userRepository.listAll()).thenReturn(singletonList(mockUser));

        List<User> users = sut.getUsers();

        assertThat(users.get(0)).isEqualTo(mockUser);

        verify(userRepository).listAll();
    }

    @Test
    void should_get_one_user() {
        User mockUser = getMockUser();
        when(userRepository.findByIdOptional(mockUser.getId())).thenReturn(Optional.of(mockUser));

        User user = sut.getUser(mockUser.getId());

        assertThat(user).isEqualTo(mockUser);
        verify(userRepository).findByIdOptional(mockUser.getId());
    }

    @Test
    void should_throw_when_user_not_found() {
        String userId = "lorem-ipsum";
        when(userRepository.findByIdOptional(userId)).thenReturn(Optional.empty());

        NotFoundException exception = assertThrows(NotFoundException.class, () -> sut.getUser(userId));
        assertThat(exception.getMessage()).isEqualTo("User with id 'lorem-ipsum' not found");

        verify(userRepository).findByIdOptional(userId);
    }

    @Test
    void should_update_one_user() {
        User initialUser = spy(getMockUser());
        User updateUser = new User();
        updateUser.setId(initialUser.getId());
        updateUser.setEmail("new " + initialUser.getEmail());
        updateUser.setFullname("new " + initialUser.getFullname());
        updateUser.setAddress("new " + initialUser.getAddress());
        updateUser.setZipcode("new " + initialUser.getZipcode());
        updateUser.setCity("new " + initialUser.getCity());


        when(userRepository.findByIdOptional(initialUser.getId(), PESSIMISTIC_WRITE)).thenReturn(Optional.of(initialUser));
        Mockito.doNothing().when(initialUser).persist();

        User user = sut.update(updateUser);

        assertThat(initialUser).isEqualTo(user);

        assertThat(user.getId()).isEqualTo(initialUser.getId());
        assertThat(user.getEmail()).isEqualTo(initialUser.getEmail());
        assertThat(user.getFullname()).isEqualTo(updateUser.getFullname());
        assertThat(user.getAddress()).isEqualTo(updateUser.getAddress());
        assertThat(user.getZipcode()).isEqualTo(updateUser.getZipcode());
        assertThat(user.getCity()).isEqualTo(updateUser.getCity());

        verify(userRepository).findByIdOptional(initialUser.getId(), PESSIMISTIC_WRITE);
        verify(userRepository).save(initialUser);
    }

    @Test
    void should_throw_on_update_when_user_not_found() {
        User initialUser = spy(getMockUser());

        when(userRepository.findByIdOptional(initialUser.getId(), PESSIMISTIC_WRITE)).thenReturn(Optional.empty());
        Mockito.doNothing().when(initialUser).persist();

        assertThrows(NotFoundException.class, () -> sut.update(initialUser));

        verify(userRepository).findByIdOptional(initialUser.getId(), PESSIMISTIC_WRITE);
    }

    @Test
    void should_register_user() {
        UserAuthDto userToCreate = aUserAuthDto();
        userToCreate.setPassword("bacon");


        User user = sut.register(userToCreate);

        verify(userRepository).save(userCaptor.capture());
        User persistedUser = userCaptor.getValue();

        assertThat(UUID.fromString(persistedUser.getId())).isNotNull();
        assertThat(persistedUser.getRoles()).isEqualTo(USER_ROLE);
        assertThat(persistedUser.getEmail()).isEqualTo(userToCreate.getEmail());
        assertThat(persistedUser.getPassword()).isNotEqualTo(userToCreate.getPassword());
        assertThat(BCrypt.checkpw(userToCreate.getPassword(), persistedUser.getPassword())).isTrue();
        assertThat(user).isEqualTo(persistedUser);
    }

    @Test
    void should_generate_jwt_token() throws Exception {
        UserAuthDto dto = aUserAuthDto();
        User aUser = sut.register(dto);
        when(userRepository.findByEmail(dto.getEmail())).thenReturn(Optional.of(aUser));

        String token = sut.getToken(dto);

        JWTCallerPrincipal parsed = parse(token);
        assertThat(parsed.getSubject()).isEqualTo(aUser.getId());
        long minExpirationDate = LocalDate.now().plusDays(364).toEpochSecond(LocalTime.NOON, ZoneOffset.MIN);
        assertThat(parsed.getExpirationTime()).isGreaterThan(minExpirationDate);
        assertThat(parsed.getIssuer()).isEqualTo("ecommerce.ffhs.novarx.ch");
        assertThat(parsed.getGroups()).containsOnly(USER_ROLE);
    }

    @Test
    void should_generate_admin_jwt_token() throws Exception {
        UserAuthDto dto = aUserAuthDto();
        User aUser = sut.register(dto);
        aUser.setRoles(aUser.getRoles() + "," + ADMIN_ROLE);
        when(userRepository.findByEmail(dto.getEmail())).thenReturn(Optional.of(aUser));

        String token = sut.getToken(dto);

        JWTCallerPrincipal parsed = parse(token);
        assertThat(parsed.getSubject()).isEqualTo(aUser.getId());
        assertThat(parsed.getGroups()).containsOnly(USER_ROLE, ADMIN_ROLE);
    }

    private UserAuthDto aUserAuthDto() {
        UserAuthDto dto = new UserAuthDto();
        dto.setEmail("lorem@ipsum.org");
        return dto;
    }

    @Test
    void should_throw_on_not_found_user() {
        UserAuthDto dto = aUserAuthDto();
        when(userRepository.findByEmail(dto.getEmail())).thenReturn(Optional.empty());

        assertThrows(UnauthorizedException.class, () -> sut.getToken(dto));

        verify(userRepository).findByEmail(dto.getEmail());
    }

    @Test
    void should_throw_on_invalid_password() {
        UserAuthDto dto = aUserAuthDto();
        User aUser = sut.register(dto);
        dto.setPassword("WRONG");
        when(userRepository.findByEmail(dto.getEmail())).thenReturn(Optional.of(aUser));

        assertThrows(UnauthorizedException.class, () -> sut.getToken(dto));

        verify(userRepository).findByEmail(dto.getEmail());
    }

    JWTCallerPrincipal parse(String token) throws Exception {
        // Token has already been verified, parse the token claims only
        String json = new String(Base64.getUrlDecoder().decode(token.split("\\.")[1]), StandardCharsets.UTF_8);
        return new DefaultJWTCallerPrincipal(JwtClaims.parse(json));
    }

    @Test
    void should_delete_user() {
        String userId = "lorem-ipsum";

        sut.deleteUser(userId);

        verify(basketService).removeAllArticles(userId);
        verify(userRepository).delete("id", userId);
    }

    private User getMockUser() {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setEmail("info@example.org");
        user.setFullname("Hans Wurst");
        return user;
    }

}
