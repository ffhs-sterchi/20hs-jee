package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.control.event.RestEventService;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.reactivex.subjects.BehaviorSubject;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import reactor.test.StepVerifier;

import javax.inject.Inject;

import static java.time.Duration.ofMillis;
import static org.mockito.Mockito.when;

@QuarkusTest
class RecommendationResourceTest {

    @Inject
    RecommendationResource sut;

    @InjectMock
    RestEventService eventService;

    @Test
    void should_publish_last_viewed_article_every_2s() {
        BehaviorSubject<String> subject = BehaviorSubject.createDefault("lorem");
        when(eventService.getLastViewedArticle()).thenReturn(subject);

        Publisher<String> publisher = sut.greetingsAsStream();

        StepVerifier.create(publisher)
            .expectNext("lorem")
            .then(() -> subject.onNext("lorem2"))
            .expectNoEvent(ofMillis(1990))
            .expectNext("lorem2")
            .thenCancel()
            .verifyThenAssertThat(ofMillis(2020));
    }
}
