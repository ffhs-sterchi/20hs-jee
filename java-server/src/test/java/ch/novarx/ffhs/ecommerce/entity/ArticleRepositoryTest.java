package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@QuarkusTest
class ArticleRepositoryTest {

    @InjectSpy
    ArticleRepository sut;

    @Test
    void should_getByIds() {
        Map<Long, Article> articleMap = sut.getByIds(List.of(1L, 1L, 2L));

        assertThat(articleMap).hasSize(2);
        assertThat(articleMap.keySet()).containsOnly(1L, 2L);
        assertThat(articleMap.values().stream().map(a -> a.getId())).containsOnly(1L, 2L);
    }

    @Test
    void should_create() {
        Article article = TestUtil.aArticle();
        doNothing().when(sut).persist(article);
        sut.save(article);
        verify(sut).persist(article);
    }

    @Test
    void should_delete() {
        sut.delete(1L);
        verify(sut).delete("id", 1L);
    }
}
