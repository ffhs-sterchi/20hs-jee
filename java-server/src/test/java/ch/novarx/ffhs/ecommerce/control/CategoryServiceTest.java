package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.boundary.dto.CategoryDto;
import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.ArticleRepository;
import ch.novarx.ffhs.ecommerce.entity.Category;
import ch.novarx.ffhs.ecommerce.entity.CategoryRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ch.novarx.ffhs.ecommerce.entity.TestUtil.aArticle;
import static java.util.stream.Collectors.toList;
import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class CategoryServiceTest {
    @Inject
    CategoryService sut;

    @InjectMock
    CategoryRepository categoryRepository;

    @InjectMock
    ArticleRepository articleRepository;

    private ArgumentCaptor<Category> captor = ArgumentCaptor.forClass(Category.class);

    @Test
    void should_throw_when_category_not_found() {
        long categoryId = 55L;
        when(categoryRepository.findByIdOptional(categoryId)).thenReturn(Optional.empty());

        NotFoundException exception = assertThrows(NotFoundException.class, () -> sut.getCategory(categoryId));
        assertThat(exception.getMessage()).isEqualTo("Category with id '55' not found");

        verify(categoryRepository).findByIdOptional(categoryId);
    }

    @Test
    void should_get_an_entity() {
        long categoryId = 55L;
        Category category = aCategory();
        when(categoryRepository.findByIdOptional(categoryId)).thenReturn(Optional.of(category));

        CategoryDto readCategory = sut.getCategory(categoryId);

        assertThat(readCategory.getName()).isEqualTo(category.getName());
        assertThat(readCategory.getDescription()).isEqualTo(category.getDescription());
        assertThat(readCategory.getArticles()).hasSize(1);
        assertThat(readCategory.getArticles().get(0).getId()).isEqualTo(2L);
        verify(categoryRepository).findByIdOptional(categoryId);
    }

    @Test
    void should_get_entities() {
        Category category = aCategory();
        when(categoryRepository.listAll()).thenReturn(List.of(category));

        List<CategoryDto> response = sut.getCategories();

        assertThat(response).hasSize(1);
        CategoryDto readCategory = response.get(0);

        assertThat(readCategory.getName()).isEqualTo(category.getName());
        assertThat(readCategory.getDescription()).isEqualTo(category.getDescription());
        assertThat(readCategory.getArticles()).isNull();
        verify(categoryRepository).listAll();
    }

    @Test
    void should_create_an_entity() {
        CategoryDto dto = CategoryDto.of(aCategory());

        CategoryDto categoryCreated = sut.createCategory(dto);

        verify(categoryRepository).persist(captor.capture());
        assertThat(categoryCreated.getName()).isEqualTo(dto.getName());
        assertThat(categoryCreated.getDescription()).isEqualTo(dto.getDescription());
        assertThat(categoryCreated.getArticles()).isNull();
        assertThat(categoryCreated).isEqualToIgnoringGivenFields(captor.getValue(), "id", "articles");
    }

    @Test
    @Transactional
    void should_update_an_entity() {
        Category category = aCategory();
        CategoryDto dto = CategoryDto.ofWithArticles(category);
        when(categoryRepository.findById(dto.getId(), PESSIMISTIC_WRITE)).thenReturn(category);

        sut.updateCategory(dto.getId(), dto);

        verify(categoryRepository).persist(captor.capture());

        assertThat(captor.getValue().getId()).isEqualTo(dto.getId());
        assertThat(captor.getValue().getName()).isEqualTo(dto.getName());
        assertThat(captor.getValue().getDescription()).isEqualTo(dto.getDescription());
        assertThat(captor.getValue().getArticles()).isEqualTo(category.getArticles());
        assertThat(captor.getValue()).isEqualToIgnoringGivenFields(captor.getValue(), "id", "articles");
    }

    @Test
    @Transactional
    void should_remove_articles_to_category() {
        Category category = aCategory();
        List<Article> articlesToRemove = getArticles();
        category.getArticles().addAll(articlesToRemove);
        List<Long> articleIdsToRemove = articlesToRemove.stream().map(a -> a.getId()).collect(toList());

        when(categoryRepository.findById(category.getId())).thenReturn(category);

        sut.removeArticles(category.getId(), articleIdsToRemove);

        assertThat(category.getArticles()).hasSize(1).doesNotContainAnyElementsOf(articlesToRemove);
        verify(categoryRepository).persist(category);
    }

    @Test
    @Transactional
    void should_add_articles_to_category() {
        Category category = aCategory();
        List<Article> newArticles = getArticles();
        List<Long> newArticlesIds = newArticles.stream().map(a -> a.getId()).collect(toList());

        when(categoryRepository.findById(category.getId())).thenReturn(category);
        when(articleRepository.list("id in ?1", newArticlesIds)).thenReturn(newArticles);

        sut.addArticles(category.getId(), newArticlesIds);

        assertThat(category.getArticles()).hasSize(3).containsAll(newArticles);
        verify(categoryRepository).persist(category);
    }

    private Category aCategory() {
        Category category = new Category();
        category.setId(55L);
        category.setName("lorem");
        category.setDescription("Ipsum");
        category.getArticles().add(aArticle());
        return category;
    }

    private List<Article> getArticles() {
        ArrayList<Article> articles = new ArrayList<>();
        Article article1 = aArticle();
        article1.setId(66L);
        Article article2 = aArticle();
        article2.setId(77L);
        articles.add(article1);
        articles.add(article2);
        return articles;
    }

}
