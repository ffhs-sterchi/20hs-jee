package ch.novarx.ffhs.ecommerce.control;

import ch.novarx.ffhs.ecommerce.entity.Article;
import ch.novarx.ffhs.ecommerce.entity.ArticleRepository;
import ch.novarx.ffhs.ecommerce.entity.BasketArticle;
import ch.novarx.ffhs.ecommerce.entity.BasketArticleRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.assertj.core.api.ListAssert;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class BasketServiceTest {

    @InjectMock
    ArticleRepository articleRepository;

    @InjectMock
    BasketArticleRepository basketArticleRepository;

    @Inject
    BasketService sut;
    private String A_USER_ID = "a-user-id";

    @Test
    void should_getBasket_for_user_id() {
        BasketArticle basketArticle = aBasketArticle();
        Article article = aArticle();

        when(basketArticleRepository.getByUserId(A_USER_ID)).thenReturn(List.of(basketArticle, basketArticle));
        when(articleRepository.getByIds(anyList())).thenReturn(Map.of(article.getId(), article));

        assertBasket().hasSize(2);
        verify(basketArticleRepository).getByUserId(A_USER_ID);
        verify(articleRepository).getByIds(argThat(id -> id.contains(article.getId())));
    }

    private Article aArticle() {
        Article article = new Article();
        article.setId(1L);
        return article;
    }

    private BasketArticle aBasketArticle() {
        BasketArticle basketArticle = new BasketArticle();
        basketArticle.setUserId(A_USER_ID);
        basketArticle.setArticleId(1L);
        return basketArticle;
    }

    private BasketArticle putBasketArticle() {
        BasketArticle basketArticle = aBasketArticle();
        sut.addArticle(A_USER_ID, basketArticle.getArticleId());
        return basketArticle;
    }

    @Test
    void should_putArticle_to_basket() {
        BasketArticle basketArticle = putBasketArticle();
        when(basketArticleRepository.getByUserId(A_USER_ID)).thenReturn(singletonList(basketArticle));
        verify(basketArticleRepository).addArticle(A_USER_ID, basketArticle.getArticleId());
    }

    private ListAssert<Article> assertBasket() {
        return assertThat(sut.getBasket(A_USER_ID));
    }

    @Test
    void should_remove_an_article_form_basket() {
        Long articleId = 1L;
        sut.removeArticle(A_USER_ID, articleId);
        verify(basketArticleRepository).removeArticle(A_USER_ID, articleId);
    }

    @Test
    void should_remove_all_articles_form_basket() {
        sut.removeAllArticles(A_USER_ID);
        verify(basketArticleRepository).removeAllArticles(A_USER_ID);
    }

}
