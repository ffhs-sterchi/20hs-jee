package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class CategoryIT {

    @Inject
    ArticleRepository articleRepository;

    @Test
    @Transactional
    void should_save_a_category() {
        Article article = articleRepository.findById(2L);

        Category category = new Category();
        category.setName("A Category");
        category.setDescription("Lorem Ipsum");
        category.setArticles(List.of(article));

        category.persistAndFlush();

        List<Category> all = Category.listAll();
        all.sort(Comparator.comparingLong(c -> c.getId()));
        Category savedCategory = all.get(all.size() - 1);

        assertThat(savedCategory.getId()).isPositive();
        assertThat(savedCategory.getName()).isEqualTo(category.getName());
        assertThat(savedCategory.getDescription()).isEqualTo(category.getDescription());

        assertThat(savedCategory.getArticles()).hasSize(1);
        assertThat(savedCategory.getArticles().get(0).getId()).isEqualTo(article.getId());
    }

}
