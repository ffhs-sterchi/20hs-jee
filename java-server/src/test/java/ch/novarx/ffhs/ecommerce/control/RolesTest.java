package ch.novarx.ffhs.ecommerce.control;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class RolesTest {
    @Test
    void should_contain_two_roles() {
        assertThat(Roles.USER_ROLE).isEqualTo("user");
        assertThat(Roles.ADMIN_ROLE).isEqualTo("admin");
    }
}
