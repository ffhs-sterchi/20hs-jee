package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.base.BaseRestResourceTest;
import ch.novarx.ffhs.ecommerce.boundary.dto.UserAuthDto;
import ch.novarx.ffhs.ecommerce.entity.TestUtil;
import ch.novarx.ffhs.ecommerce.entity.User;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static ch.novarx.ffhs.ecommerce.control.Roles.USER_ROLE;
import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@QuarkusTest
@Tag("integration")
class UserResourceIT extends BaseRestResourceTest {

    @Test
    @TestSecurity(user = USER_ID, roles = USER_ROLE)
    void should_create_and_update_user() {
        User user = createUser();
        stubValidUser(user.getId());

        given().when()
            .contentType(APPLICATION_JSON)
            .body(user)
            .put("/user/me")
            .then()
            .statusCode(200);
    }

    private User createUser() {
        User user = TestUtil.aUser();
        UserAuthDto userAuthDto = new UserAuthDto();
        userAuthDto.setEmail(user.getEmail());
        userAuthDto.setPassword(user.getPassword());

        user.setId(given().when()
            .contentType(APPLICATION_JSON)
            .body(userAuthDto)
            .post("/user/register")
            .then()
            .statusCode(200)
            .extract().body().path("id").toString());
        return user;
    }
}
