package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.test.junit.QuarkusTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@QuarkusTest
class BasketArticleRepositoryTest {

    @Inject
    BasketArticleRepository sut;
    private String A_USER_ID = "a-user-id";

    @BeforeEach
    void setup() {
        sut.removeAllArticles(A_USER_ID);
        assertUserBasketArticleSize(0);
    }

    @Test
    void should_add_remove_and_get_basketArticles() {
        List<BasketArticle> articles;
        assertUserBasketArticleSize(0);

        sut.addArticle(A_USER_ID, 1L);
        assertUserBasketArticleSize(1);

        sut.addArticle(A_USER_ID, 1L);
        assertUserBasketArticleSize(2);

        sut.addArticle(A_USER_ID, 2L);
        articles = assertUserBasketArticleSize(3);

        assertThat(articles.stream().map(a -> a.getArticleId())).containsExactlyInAnyOrder(1L, 1L, 2L);
        assertThat(articles.stream().map(a -> a.getUserId())).containsOnly(A_USER_ID);

        sut.removeArticle(A_USER_ID, 1L);
        articles = assertUserBasketArticleSize(2);

        assertThat(articles.stream().map(a -> a.getArticleId())).containsExactlyInAnyOrder(1L, 2L);
        assertThat(articles.stream().map(a -> a.getUserId())).containsOnly(A_USER_ID);
    }

    @Test
    void should_throw_when_article_not_found() {
        NotFoundException exception = assertThrows(NotFoundException.class, () -> sut.removeArticle(A_USER_ID, 9999L));
        assertThat(exception.getMessage()).isEqualTo("Basket Article '9999' not found");
    }

    @Test
    void should_remove_all_articles_form_basket() {
        sut.addArticle(A_USER_ID, 1L);
        sut.addArticle(A_USER_ID, 1L);
        sut.addArticle(A_USER_ID, 2L);
        sut.addArticle(A_USER_ID, 3L);
        assertUserBasketArticleSize(4);

        sut.removeAllArticles(A_USER_ID);

        assertUserBasketArticleSize(0);
    }

    private List<BasketArticle> assertUserBasketArticleSize(int expectedSize) {
        List<BasketArticle> articles = sut.getByUserId(A_USER_ID);
        Assertions.assertThat(articles).hasSize(expectedSize);
        return articles;
    }
}
