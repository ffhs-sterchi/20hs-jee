package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.dto.ArticleDto;
import ch.novarx.ffhs.ecommerce.boundary.dto.CategoryDto;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.*;

@QuarkusTest
@Tag("integration")
class CategoryResourceIT {

    @Test
    void should_get_categories() {
        given().when()
            .get("category")
            .then()
            .log().body()
            .statusCode(200)
            .body("[0].articles", nullValue());
    }

    @Test
    void should_get_a_category() {
        given().when()
            .get("category/1")
            .then()
            .log().body()
            .statusCode(200)
            .body("articles", notNullValue());
    }

    @Test
    void should_404_when_category_not_found() {
        given().when()
            .get("category/99999")
            .then()
            .log().everything()
            .statusCode(404);
    }

    @Test
    void should_update_a_category() {
        CategoryDto category = createCategory();
        Long id = category.getId();
        category.setId(999L);
        category.setName("A new Name");
        category.setDescription("A new Description");
        category.setArticles(singletonList(new ArticleDto()));

        given().when()
            .contentType(APPLICATION_JSON)
            .body(category)
            .put("category/" + id)
            .then()
            .log().body()
            .statusCode(204);

        given().when()
            .get("category/" + id)
            .then()
            .log().body()
            .statusCode(200)
            .body("id", is(id.intValue()))
            .body("name", is(category.getName()))
            .body("description", is(category.getDescription()))
            .body("articles", empty());
    }

    private void addArticles(CategoryDto category, List<Long> articleIds) {
        given().when()
            .contentType(APPLICATION_JSON)
            .body(articleIds)
            .post("category/" + category.getId() + "/article")
            .then()
            .log().body()
            .statusCode(204);
    }

    @Test
    void should_remove_article_to_a_category() {
        CategoryDto category = createCategory();
        List<Long> articleIds = List.of(1L, 2L);
        addArticles(category, articleIds);

        given().when()
            .contentType(APPLICATION_JSON)
            .body(singletonList(articleIds.get(0)))
            .delete("category/" + category.getId() + "/article")
            .then()
            .log().all()
            .statusCode(204);

        given().when()
            .get("category/" + category.getId())
            .then()
            .log().body()
            .statusCode(200)
            .body("articles", hasSize(1))
            .body("articles[0].id", is(articleIds.get(1).intValue()));
    }

    @Test
    void should_create_a_category() {
        CategoryDto response = createCategory();
        verifyResponse(response);
    }

    private void verifyResponse(CategoryDto response) {
        given().when()
            .get("category/" + response.getId())
            .then()
            .log().body()
            .statusCode(200)
            .body("id", is(response.getId().intValue()))
            .body("name", is(response.getName()))
            .body("description", is(response.getDescription()))
            .body("articles", empty());
    }

    private CategoryDto createCategory() {
        CategoryDto dto = new CategoryDto();
        dto.setName("lorem");
        dto.setDescription("Ipsum");

        return given().when()
            .contentType(APPLICATION_JSON)
            .body(dto)
            .post("category")
            .then()
            .log().body()
            .statusCode(201)
            .body("id", greaterThan(0))
            .body("name", is(dto.getName()))
            .body("description", is(dto.getDescription()))
            .body("articles", nullValue())
            .extract().body().as(CategoryDto.class);
    }
}
