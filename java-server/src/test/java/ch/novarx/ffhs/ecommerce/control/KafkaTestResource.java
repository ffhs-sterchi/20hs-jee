package ch.novarx.ffhs.ecommerce.control;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;

import java.util.HashMap;
import java.util.Map;

import static ch.novarx.ffhs.ecommerce.control.event.RestEventService.INCOMING_NAME;
import static ch.novarx.ffhs.ecommerce.control.event.RestEventService.OUTGOING_NAME;

/**
 * Use the in-memory connector to avoid having to use a broker.
 * Described in https://github.com/quarkusio/quarkus/issues/6427
 */
public class KafkaTestResource implements QuarkusTestResourceLifecycleManager {

    @Override
    public Map<String, String> start() {
        Map<String, String> env = new HashMap<>();
        env.putAll(InMemoryConnector.switchIncomingChannelsToInMemory(INCOMING_NAME));
        env.putAll(InMemoryConnector.switchOutgoingChannelsToInMemory(OUTGOING_NAME));
        return env;
    }

    @Override
    public void stop() {
        InMemoryConnector.clear();
    }
}
