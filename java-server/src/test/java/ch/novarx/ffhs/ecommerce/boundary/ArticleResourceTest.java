package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.base.BaseRestResourceTest;
import ch.novarx.ffhs.ecommerce.control.ArticleService;
import ch.novarx.ffhs.ecommerce.entity.Article;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import static ch.novarx.ffhs.ecommerce.entity.TestUtil.aArticle;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@QuarkusTest
class ArticleResourceTest extends BaseRestResourceTest {

    @InjectMock
    ArticleService articleService;

    @Inject
    ArticleResource sut;

    @AfterEach
    void teardown() {
        verifyNoMoreInteractions(articleService);
    }

    @Test
    void should_get_article() {
        Article article = aArticle();
        when(articleService.getArticle(1L)).thenReturn(article);

        assertThat(sut.getArticle(1L)).isEqualTo(article);
        verify(articleService).getArticle(1L);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_get_all_articles() {
        Article article = aArticle();
        when(articleService.getAll()).thenReturn(singletonList(article));

        assertThat(sut.getArticles()).containsOnly(article);
        verify(articleService).getAll();
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_delete_article() {
        sut.deleteArticle(1L);
        verify(articleService).delete(1L);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_update_article() {
        Article article = aArticle();
        sut.updateArticle(article);
        verify(articleService).update(article);
    }

    @Test
    @TestSecurity(user = "aAdmin", roles = "admin")
    void should_create_article() {
        Article article = aArticle();
        Response response = sut.createArticle(article);

        assertThat(response.readEntity(Article.class)).usingRecursiveComparison().isEqualTo(article);
        assertThat(response.getStatus()).isEqualTo(201);
        verify(articleService).create(article);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_throw_on_protected_endpoints_when_no_admin() {
        assertForbidden(
            () -> sut.getArticles(),
            () -> sut.deleteArticle(1L),
            () -> sut.updateArticle(aArticle()),
            () -> sut.createArticle(aArticle())
        );
    }
}
