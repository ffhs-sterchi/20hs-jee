package ch.novarx.ffhs.ecommerce.boundary;

import ch.novarx.ffhs.ecommerce.boundary.dto.ArticleDto;
import ch.novarx.ffhs.ecommerce.control.BasketService;
import ch.novarx.ffhs.ecommerce.entity.Article;
import io.quarkus.security.UnauthorizedException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@QuarkusTest
class BasketResourceTest {

    @InjectMock
    JsonWebToken jwt;

    @InjectMock
    BasketService basketService;

    @Inject
    BasketResource sut;

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_get_basket_of_user() {
        String userId = stubAuthorizedUserAndGetId();
        Article article = aArticle();
        when(basketService.getBasket(anyString())).thenReturn(List.of(article));

        List<ArticleDto> basket = sut.getBasket();

        assertThat(basket.stream().map(a -> a.getId())).containsOnly(article.getId());
        verify(basketService).getBasket(userId);
        verify(jwt).getSubject();
    }

    @Test
    void should_not_get_basket_when_unauthenticated() {
        assertThrows(UnauthorizedException.class, () -> sut.getBasket());
        verifyNoMoreInteractions(basketService, jwt);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_add_article_to_basket() {
        String userId = stubAuthorizedUserAndGetId();
        Article article = aArticle();
        when(basketService.getBasket(anyString())).thenReturn(List.of(article));

        List<ArticleDto> basket = sut.addToBasket(article.getId());

        verify(basketService).addArticle(userId, article.getId());
        assertThat(basket.stream().map(a -> a.getId())).containsOnly(article.getId());
        verify(basketService).getBasket(userId);
        verify(jwt, times(2)).getSubject();
    }

    @Test
    void should_not_add_article_to_basket_when_unauthenticated() {
        assertThrows(UnauthorizedException.class, () -> sut.addToBasket(1L));
        verifyNoMoreInteractions(basketService, jwt);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_remove_a_article_form_basket() {
        String userId = stubAuthorizedUserAndGetId();
        Article article = aArticle();
        when(basketService.getBasket(anyString())).thenReturn(List.of(article));

        List<ArticleDto> basket = sut.removeArticle(1L);

        assertThat(basket).hasSize(1);
        verify(basketService).removeArticle(userId, 1L);
        verify(basketService).getBasket(userId);
        verify(jwt, times(2)).getSubject();
    }

    @Test
    void should_not_rm_article_from_basket_when_unauthenticated() {
        assertThrows(UnauthorizedException.class, () -> sut.removeArticle(1L));
        verifyNoMoreInteractions(basketService, jwt);
    }

    @Test
    @TestSecurity(user = "aUser", roles = "user")
    void should_remove_all_articles_form_basket() {
        String userId = stubAuthorizedUserAndGetId();
        when(basketService.getBasket(anyString())).thenReturn(emptyList());

        List<ArticleDto> basket = sut.removeArticles();

        assertThat(basket).isEmpty();
        verify(basketService).removeAllArticles(userId);
        verify(basketService).getBasket(userId);
        verify(jwt, times(2)).getSubject();
    }

    @Test
    void should_not_rm__all_articles_from_basket_when_unauthenticated() {
        assertThrows(UnauthorizedException.class, () -> sut.removeArticles());
        verifyNoMoreInteractions(basketService, jwt);
    }

    private Article aArticle() {
        Article article = new Article();
        article.setId(55L);
        return article;
    }

    private String stubAuthorizedUserAndGetId() {
        String userId = "a-user-id";
        when(jwt.getSubject()).thenReturn(userId);
        return userId;
    }
}
