package ch.novarx.ffhs.ecommerce.entity;

import ch.novarx.ffhs.ecommerce.control.Roles;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class UserIT {

    @Test
    @Transactional
    void should_save_a_user() {
        User user = new User();
        user.setId("lorem-ipsum1");
        user.setEmail("info2@example.org");
        user.setPassword("a-password*");
        user.setFullname("Hans Wurst");
        user.setAddress("Main Street 1");
        user.setZipcode("3000");
        user.setCity("Bern");
        user.setRoles(Roles.USER_ROLE);

        user.persistAndFlush();

        List<User> all = User.listAll();

        User savedUser = all.stream().filter(u -> u.getId().equals(user.getId())).findFirst().orElseThrow();

        assertThat(savedUser.getEmail()).isEqualTo(user.getEmail());
        assertThat(savedUser.getFullname()).isEqualTo(user.getFullname());
        assertThat(savedUser.getPassword()).isEqualTo(user.getPassword());
        assertThat(savedUser.getAddress()).isEqualTo(user.getAddress());
        assertThat(savedUser.getZipcode()).isEqualTo(user.getZipcode());
        assertThat(savedUser.getCity()).isEqualTo(user.getCity());
    }

}
