package ch.novarx.ffhs.ecommerce.entity;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@QuarkusTest
class OrderRepositoryTest {

    @InjectSpy
    OrderRepository sut;

    @Test
    void should_get_all_orders_by_userid() {
        String userId = "a-user-id";
        ArrayList<Order> list = new ArrayList<>();
        when(sut.list("userId", userId)).thenReturn(list);

        List<Order> orders = sut.listByUserId(userId);
        Assertions.assertThat(orders).isEqualTo(list);
        verify(sut).list("userId", userId);
    }

    @Test
    void should_create_and_read_order() {
        Order order = TestUtil.aOrder();
        sut.save(order);

        Order persistedOrder = sut.findById(order.getId());

        assertThat(persistedOrder.getId()).isEqualTo(order.getId());
        assertThat(persistedOrder.getUserId()).isEqualTo(order.getUserId());
        assertThat(persistedOrder.getOrderDate()).isEqualTo(order.getOrderDate());
        assertThat(persistedOrder.getArticles()).hasSameSizeAs(order.getArticles());
        assertThat(persistedOrder.getArticles().get(0).getId()).isEqualTo(order.getArticles().get(0).getId());
    }
}
