# Implementation

Die meisten Stories konnten gut umgesetzt werden, aber Details zum Verlauf der Sprints finden sich im Kapitel "Retorspektive".

## Effektive Architektur

```plantuml
@startuml
skinparam shadowing false
skinparam ArrowColor DarkSlateGrey
skinparam agent {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
skinparam queue {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}

package Quarkus-Server {
    package boundary {
        agent RestResource
        agent DataTransferObject
    }
    package control {
        agent BuisnessLogic
    }
    package entity {
        agent DatabaseAccess
        agent DataModel
    }
    queue EventQueue
    
    DataTransferObject o- RestResource
    
    RestResource --> EventQueue
    RestResource --> BuisnessLogic
    EventQueue o- BuisnessLogic
    
    BuisnessLogic --> DatabaseAccess
    DataModel o- DatabaseAccess
}

package Angular-Client {
    agent ViewComponent
    agent IntegrationService
}
RestResource <- IntegrationService
IntegrationService <-- ViewComponent
@enduml
```

## Quarkus
Die effektive Implementation des Online-Shops wurde mithilfe dem Java-Framwork _Quarkus_ [^quarkus] realisiert.
Quarkus baut in vielen Teilen auf JEE auf, so dass es eine simple Alternative zu den teils eher schwerfälligen JEE-Servern bietet.

Quarkus verwendet also z.B. für die Dependency-Injection auch CDI [^cdi] oder für die Rest-Resourcen JAX-RS[^jaxrs] welches beides JEE-Spezifikationen sind.

Zusätzlich zu regulären JEE-Spezifikationen bietet Quarkus auch viele Integrationen zu anderen gängigen Tools[^quarkus_guides] und Frameworks fast per Knopfdruck an.
Die initiale Erstellung eines Projekt so einfach wie fast nirgends und kann via online Builder per Knopfdruck gemacht werden [^quarkus_generator].

Für die Entwicklung wohl das praktischste Feature von Quarkus ist das Live-Reloading des Codes. Wenn also die Entwicklungsinstanz von Quarkus lokal am laufen ist (wird mit einem einfache Maven Befehl gestartet). So kann man einen beliebigen Teil des Codes verändern und ohne ein Redeployment oder Restart des Servers einen neuen Request absetzen
und der Code wird selbstständig Nachgeladen und ausgeführt.

## Datenhaltung

Für die Entwicklung habe ich ausschliesslich eine H2-Datenbank [^h2] verwendet, das ist eine in-memory Datenbank. Beim starten der Applikation wird diese also immer neu mittels Flyway [^flyway] initialisiert. 

## Webshop lokal starten

Um den Webshop lokal zu starten kann man im Root-Verzeichnis des Projektes[^git_repo] den Befehl `docker-compose up` ausführen und kurz warten bis die Container laufen (wenn die Konsole nicht mehr permanent Zeilen loggt).

Das Shop-Frontend ist erreichbar unter: **http://localhost:4200/**

Ein Swagger-UI [^swagger] für das Backend ist erreichbar unter: **http://localhost:8080/swagger-ui/**

## Testing

Für Implementierung des Backends habe ich Test-Driven gearbeitet, was heisst vor dem schreiben einer Codezeile im "produktiven" Code habe ich den entsprechenden Unittest dazu geschrieben. [^tdd] Durch dieses Vorgehen konnte ohne Probleme eine Unit-Test Zeilenabdeckung von über 98% erreicht werden.

Da es in diesem Projekt um JEE geht, habe ich auf ein Unit-Testing des Frontends verzichtet. Jedoch gibt es einige E2E-Tests, welche die Integration zum Backend testen. Diese fleissen jedoch nicht etwa in die Coverage und laufen leider auch nicht in der Pipeline.

Die Unit-Testabdeckung des Master-Branch des Backends kann unter **https://ffhs-sterchi.gitlab.io/20hs-jee/coverage** eingesehen werden.


## Known-Bugs

Es gibt natürlich diverse Dinge, die aufgrund der Zeit nicht realisiert werden konnten oder aufgrund der Tatsache, dass es sich mehr um einen MVC bzw. POC handelt, absichtlich nicht mit allen Details gemacht wurden.

### Artikelverwaltung

Die Artikelverwaltung ist sehr spärlich gehalten, validiert wird nicht viel und C(R)UD-Funktioinen sind eigentlich nur via Swagger, CURL oder ähnlichem machbar. Auch die Lagerhaltung muss komplett manuel via des Update-Endpoints eines Artikels gemacht werden. Zudem wird der Lagerbestand auch nicht im Shop selbst angezeigt. Hier kann man allerdings disskutieren ob das nicht gewollt sein könnte...

### Lagerverwaltung

Die Lagerverwaltung ist sehr rudimentär und weit davon entfernt automatisch bewirtschaftet zu werden, das müsste erweitert werden.

### Vorschläge
Die Artikel "Was sich andere Kunden ansehen" werden immer nach dem letzten Event abgewartet, so kann es vorkommen, dass nie ein neuer Artikel erscheint.


### Userverwaltung
Die Userverwaltung ist auch auf ein minimum begrenzt. Insbesondere die möglichkeit das Passwort zu ändern wäre vielleicht noch schön. Was aktuell nicht möglich ist.


### Frontend

Das Frontend ist sehr schmall gehalten, insb. ein Server-Side-Rendering müsste man noch implementieren um wirklich als Webshop verwendet zu werden. Denn von Haus aus ist eine Angular Applikation ein OnePager, also die Ganzen Inhalte werden dynamisch nachgeladen - das gefällt z.B. Google garnicht...

Das Design und auch die Funktionalitäten im UI sind auch eher dürftig. Die erste Funktion welche ich wohl nachrüsten würde ist, dass ein Warenkorb auch ohne Login verwaltet werden kann.

Auch ein Bestellen ohne User-Account wäre nett.


### Secrets
Das Private- und Public-Certificate um JWT-Token [^jwt] zu erstellen befinden sich im Repository selbst. Das müsste unbedingt seinen Platz in einer Secret-Verwaltung wie z.B. als Kubernetes Secret [^k8s-secret] geführt werden.  

### E2E-Tests
Die E2E-Tests werden nicht in der Pipeline ausgeführt, was sehr schade ist. Ich bin der Meinung Tests die nicht in der Pipeline laufen sind fast Wertlos, denn jeder Entwickler ist so selbst verantwortlich, die Tests auszuführen. Werden Sie dann ein, zwei mal nicht ausgeführt, pflegt sie plötzlich niemand mehr nach.

Ich hatte einige Stunden versucht die Cypress [^cypress] in der Pipeline laufen zu lassen, leider ohne Erfolg. 


[^tdd]: https://martinfowler.com/bliki/TestDrivenDevelopment.html
[^swagger]: https://swagger.io/
[^jwt]: https://jwt.io/
[^cdi]: http://cdi-spec.org/
[^jaxrs]: https://docs.oracle.com/javaee/6/tutorial/doc/giepu.html
[^quarkus]: https://quarkus.io/
[^quarkus_guides]: https://quarkus.io/guides/
[^quarkus_generator]: https://code.quarkus.io/
[^h2]: https://www.h2database.com/
[^flyway]: https://flywaydb.org/
[^git_repo]: https://gitlab.com/ffhs-sterchi/20hs-jee
[^k8s-secret]: https://kubernetes.io/docs/concepts/configuration/secret/
[^k8s-cypress]: https://www.cypress.io/
