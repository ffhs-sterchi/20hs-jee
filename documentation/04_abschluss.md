# Retrospektive

Nach den Sprints 2 und 3 wurde eine kleine Retrospektive gemacht, die Protokolle davon sind hier noch aufgelistet.

## Sprint 2
Im Backend lief anfangs alles ziemlich rund mit den Neuentwicklungen.

Wurde jedoch rasch etwas mühsam, weil ich im ersten Sprint für diverse Aufgaben [Panache](https://quarkus.io/guides/hibernate-orm-panache) verwendet habe.
Panache ist ein ORM welches verspricht Datenzugriffe zu vereinfachen und eine rasche Entwicklung zu fördern.

Nun ja... Panache ist wohl noch ziemlich in den Kinderschuhen und hilfe lässt sich nur schwer finden.
So bereitete mir das ORM schlussendlich mehr Probleme, als es die Entwicklung vereinfachte.

Ich habe dann begonnen einige Teile zu refactoren und Panache wieder auszubauen. Leider konnte ich noch nicht ganz alles entfernen, habe jedoch vor die restliche Überbleibsel im nächsten Sprint zu entfernen.

Weiter hat mir der Authenizierungs-Mechanismus über [Keycloak](https://www.keycloak.org/) auch etwas mehr Aufwand bereitet als gedacht. Insb. im Frontend hatte ich dort etwas zu kämpfen. Mitlerweilen funktioniert dass aber bestens.

Alles in allem ein spannender Sprint, jedoch mit einigen Hürden was Frameworks anbelangt.

Für die weiteren Sprints nehme ich mir vor auf mir unbekannte Tools & Frameworks zu verzichten =)


## Sprint 3
Die geplanten Tasks konnte ich bis jetzt weitgehends bearbeiten. Werde diese Woche noch die letzte verbleibenden Story
schliessen.

Der Webshop ist also mittlerweilen "voll" Funktionsfähig, was heisst die folgenden Epics konnten abgeschlossen werden:
- Produkteverwaltung
- Kundenverwaltung
- Warenkorb
- Bestellungen

Verbleiben also noch die beiden Fokus-Epics _Logging_ und _Vorgeschlagene Artikel_. Wobei das Logging auch so gut wie
abgeschlossen ist, dort muss nur noch Kafka konfiguriert und konsumiert werden (der letzte verbleibende Task aus diesem Sprint).

Auch in diesem Sprint haben mich die Entscheidungen aus dem 1.Sprint eingeholt. Keycloak als Auth-Server zu verwenden,
war wohl doch auch nicht so Zeitspraend wie ich mir das vorgestellt hätte. Also habe ich kurzerhand Keycloak wieder
komplett ausgebaut und die Authentisierung nachträglich noch selbst eingebaut.

Somit gibt es in der Userverwaltung nun noch die Funktionalität um sich zu registrieren und mittels Login einen JWT-Token
generieren zu lassen. Welcher dann für die mit `@RolesAllowed` geschützten Endpoints zur Authent- und Authorisierung verwendet wird.


#### Fokusthema

Die Grundlage um mich im Thema _Message-Driven_ zu vertiefen konnte ich im Sprint 4 erstellen.
Im Frontend wird nun jedem Res-Call an das Backend eine "correlation-id" zugewiesen. Das ist eine UUID welche dazu dienen
soll alle Requests eindeutig zu identifizieren. Zusätzlich werden auch alle Rest-Calls im backend "[intercepted](https://en.wikipedia.org/wiki/Interceptor_pattern)".

Im nächsten und letzten Sprint geht es nun also darum, all diese "konsumierten" Requests aus der Queue zu konsumieren.
Die konsumierten Daten sollen schliesslich verwendet werden um den Kunden anzuzeigen, was andere Kunden gerade anschauen
und / oder bestellen.

Wichtig zu erwähnen ist, dass ich bei den reaktiven/asynchronen Aufgaben wohl nicht mit den Standard JEE-Anotation `@MessageDriven`
arbeiten werde. Da in Quarkus die Verwendung "Reaktiver Programmierung" mittels [_Smallrye Messageing_](https://smallrye.io/smallrye-reactive-messaging/smallrye-reactive-messaging/2/index.html)
vereinfacht wurde, werde ich mich an diese Vorgaben ausrichten.

Die Verwendung davon ist allerdings angelehnt an die MessageDriven-Beans von JEE. Es gibt also auch einen Kanal, welchen man
konsumieren kann um reaktiv auf "Nachrichten" zu reagieren.


**Live-Feed**

Es soll also die Queue "realtime" über einen [SSE](https://en.wikipedia.org/wiki/Server-sent_events) Endpoint agregiert (insb. anonymisiert) konsumiert werden können.

Weiter sollen noch "fachliche" Events wie "Artikel bestellt" und "Artikel in Warenkorb gelegt" spezifischer in die Queue
geschrieben werden. So soll es auch möglich sein anderen Kunden anzuzeigen, was gerade so bestellt wird.
Je bestellte Bestellung soll aber nur höchstens ein Artikel ausgegeben werden.


**Weiteres**

Sollte die Zeit noch reichen um weitere "Gebiete" zu erforschen, werde ich noch versuchen, einen Algorithmus zu errarbeiten,
welcher anhand von anderen User-Verhalten vorschläge macht.
Wenn also ein anderer Kunde ein ähnlichen z.B. Klickverhalten hat wie ich, sollen mir vorschläge gemacht werden, welche Produkte er so
gekauft hat.

Dazu könnte die Queue konsumiert werden um z.B. die [Cosine-Similarity](https://www.sciencedirect.com/topics/computer-science/cosine-similarity)
zu berechnen. Da dies aber wohl relativ rechenintensiv ist, müssten die "Zwischenresultate" vielleicht in einer Zwischentabelle persistiert werden.

Stories dazu sind bereits aufgenommen.


\newpage

# Litaraturverzeichnis {.unnumbered}
