# Spezifikation Online-Shop

## Anforderungen
Es soll ein Online-Shop erstellt werden welcher folgende Kriterien erfüllt:

- Verwaltung von Kunden
  - Registrierung / Deregisitrierung von Kunden
  - Anpassung der Eigenschaften von Kunden 
- Verwaltung der Einkäufe und Verkäufe pro Kunde
  - Warenkorbverwaltung pro Kunde
- Verwaltung von angebotenen Produkte pro Kunde
  -Hinzufügen und Entfernen von Produkten
  -Anpassung von Produkten (Anzahl im Lager, Name, ID, usw.)

## Epics

- Oben genannte Kern-Anforderungen werden in den folgenden Epics 01-04 abgedeckt
- Die Fokus Thematik **Message-Driven** wird mit den Epics 05-06 vertieft.

### out of scope

- Lagerverwaltung
- Einschränkung möglicher bestellbarer / anzeigbarer Artikel je Kunde(-ngruppe)
- GUI zur Verwaltung von Artikel (kann aber über Rest oder SwaggerUI gemacht werden)

## Kontext (in-scope)

Die in-scope Anforderungen könen mit folgendem Komponenten-Diagram veranschaulicht werden.

```{.plantuml width=65%}
@startuml
skinparam shadowing false
package JEE-Server as JEE {
  package Core #dfe5f0 {
    package Users {
      [UserService]
    }

    package Orders {
     [OrderService]
     [BasketService]
    }

    package BaseCatalog {
      [ArticleService]
      [CategoryService]
    }
  }
  
  package Logging {
    [LogReciver] #fff
    queue LogsTopic as Logs #fff
  }
  
  package Marketing {
    [RecommendationService] #fff
  }

}


ArticleService *-- OrderService
ArticleService *- BasketService
ArticleService *- CategoryService

OrderService -* UserService
BasketService *- OrderService

Logs <- LogReciver
RecommendationService -* Logs
RecommendationService --* ArticleService


skinparam component {
    backgroundColor<<out of scope>> LightGrey
    backgroundColor WhiteSmoke
    borderColor DarkSlateGrey
    ArrowColor DarkSlateGrey
}
skinparam queue {
    backgroundColor<<out of scope>> LightGrey
    backgroundColor WhiteSmoke
    borderColor DarkSlateGrey
    ArrowColor DarkSlateGrey
}
skinparam package {
    borderColor DarkSlateGrey
    ArrowColor DarkSlateGrey
}
@enduml
```


## Epic 01 - Produkteverwaltung

Produkte können erstellt, bearbeitet und gelöscht werden
Die Produkteverwaltung soll nur über die Rest-Schnittstelle getätigt werden können.
Es wird also kein GUI dafür geben, ggf. wird ein SwaggerUI aufgesetzt zur einfacheren Verwaltung.

### Endpoints
- `GET  /article`
- `GET  /article/{id}`
- `POST /article`
- `PUT  /article/{id}`
- `GET  /category`
- `GET  /category/{id}`
- `POST /category`
- `PUT  /category/{id}`
- `POST /category/{id}/article`
- `DELETE /category/{id}/article`

### UI-Components

Es soll eine Übersicht einer Kategorie geben, wo alle darin enthaltenen Artikel in einer Vorschau angezeigt werden.

Weiter soll wenn auf die Vorschau eines Artikels geklickt wird dessen Detail-Sicht geöffnet werden. Welche wie folgt aussehen könnte.


```plantuml
@startsalt
{^"Artikel xyz"

.|{^""
.|Bild                .
.
.
.
}
.|CHF 20.00|.
.|[<&cart> in Warenkorb]
.
.|{^"Beschreibung:"
.|Bacon ipsum dolor amet short ribs kielbasa pig meatloaf. 
.|Jowl hamburger shank frankfurter, flank ham brisket
.|capicola turkey.
.
}
}
@endsalt
```


### Klassendiagramm
```plantuml
@startuml
skinparam ArrowColor DarkSlateGrey
skinparam class {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
class Article {
  name: String
  description: String
  price: BigDecimal
  imageurl: String
}

class Category {
  name: String
  description: String
  articles: List<Article>
}

Article *- Category
@enduml
```

## Epic 02 - Kundenverwaltung

Kunden können sich registrieren, deregistrieren und anzeigen. Es gibt einen Loginmechanismus für Kunden und Administratoren.

Administratoren können alle oder einen Kunden anzeigen lassen, wobei hierfür kein GUI existieren wird (ggf. SwaggerUI).
Ein Admin kann auch einen Kunden deregistrieren.

### Endpoints
- `GET  /user`
- `GET  /user/{id}`
- `PUT  /user/{id}`
- `DELETE /user/{id}`
  - Entspricht der _deregistrierung_
  - Alle mit dem User verbundenen Warenkörbe und Bestellungen werden ebenfalls gelöscht
- `POST /register`
- `POST /login`

Alternativ, könnte auch OpenID via [Keycloak](https://github.com/keycloak/keycloak-containers) verwendet werden, siehe: https://quarkus.io/guides/security-openid-connect-web-authentication

### UI-Components
Es soll GUI Ansichten geben um sich anzumelden, abzumelden und seine eigenen Informationen zu bearbeiten. Letzteres könnte wie folgt aussehen:

```plantuml
@startsalt
{^"Benutzerkonto"
  E-Mail    |.| hans@wurst.ch
  Passwort  |.| "****         "
  Name      |.| "Hans Wurst   "
  Strasse & Nr. |.| "Weg 22       "
  PLZ & Ort | "3000" | "Bern   "
  .|.|[speichern]
}
@endsalt
```

### Klassendiagramm
```plantuml
skinparam ArrowColor DarkSlateGrey
skinparam class {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
class User {
  email: String
  password: String
  fullname: String
  address: String
  zipcode: String
  city: String
  role: Role
}

enum Role {
  ADMIN
  CUSTOMER
}

interface IBasket
interface IOrder

User *-- IBasket
User *-- IOrder
User -* Role
```

## Epic 03 - Warenkorb

Ein Kunde kann Produkte zu einem Warenkorb hinzufügen und entfernen, dieser Warenkorb kann auch angezeigt werden.

### Endpoints
- `GET    /basket`
- `PUT    /basket/article/{id}`
- `DELETE /basket/article/{id}`

### UI-Components
Eine Warenkorb-Seite, welche eine übersicht aller im Warenkorb vorhandenen Artikel zeigt.

### Klassendiagramm
```plantuml
skinparam ArrowColor DarkSlateGrey
skinparam class {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
class Basket {
  user: User
  articles: List<Article>
}

interface IArticle
interface IUser

Basket --* IArticle
Basket -* IUser
```

## Epic 04 - Bestellungen

Ein Kunde kann die Artikel in seinem Warenkorb bestellen. 
Dadurch werden alle Artikel aus dem Warenkorb in eine neue Bestellung "verschoben".

Bereits vorhandene Bestellungen können eingesehen werden.

### Endpoints
- `GET  /order`
- `POST /order`

### UI-Components
Es soll einen Button im Warenkorb geben um eben diesen zu bestellen, nach einer Bestellung soll noch eine kleine Bestätigungseite angezeigt werden.

### Klassendiagramm
```plantuml
skinparam ArrowColor DarkSlateGrey
skinparam class {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
class Order {
  user: User
  articles: List<Article>
  orderDate: Date
}

interface IArticle
interface IUser

Order --* IArticle
Order -* IUser
```

## Epic 05 - Logging

Aktivitäten sollen geloggt werden in einer einfach zu verarbeitenden Form.
Die Logs sollen direkt in eine Queue gelangen (falls möglich Kafka).

Mit Aktivitäten ist insb. gemeint:

- Artikel anschauen
- Artikel bestellen
- Kategorie anschauen
- Grundsätzlich alle REST-Calls
  - Jeder Call aus dem Frontend soll mit einer UUID versehen werden

Das Format eines solchen Log-Eintrages könnte so ähnlich sein:
```json
{
  "datetime": "2020-01-01T20:00:00",
  "eventId": "e911a8e3-c6ea-4f99-9b6f-bdbb4761587c",
  "ipAddress": "1.1.1.1",
  "user": "xyz",
  "subject": "order",
  "endpoint": null,
  "metadata": {}
}
```

## Epic 06 - Vorgeschlagene Artikel

Es soll die Logging-Queue konsumiert werden um Vorschläge für weitere "interessante Artikel" einem Kunden zu unterbreiten.

Diese Funktion kann in zwei Ausprägungen umgesetzt werden:

### v1: "Artikel xy wird gerade angeschaut"
Hier wird _realtime_ dem Kunden angezeigt, welche Produkte gerade von anderen Kunden angeschaut werden.

**Fokussierung**

- Queue konsumieren mit einem Offset x
- Neue Events / Messages werden mit [SSE](https://en.wikipedia.org/wiki/Server-sent_events) nachgeliefert

### v2: "Artikel xy könnte sie auch interessieren"
Hier werden alle Logs, welche für einen Artikelvorschlag interessant sein könnten aggregiert und über einen dedizierten Endpoint bereitgestellt.

**Fokusierung**

- Konsumieren und aggregieren von Events
- Ggf. die Aggregate in einem Cache oder weiteren Queue bereitstellen
