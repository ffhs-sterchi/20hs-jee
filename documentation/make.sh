#!/bin/bash

# Get path OS agnostic
directory_path=$(pwd) # unix
if [[ "$OSTYPE" == "msys"* ]]; then
    directory_path=$(pwd -W) # windows
fi

echo "directory_path is: $directory_path"

docker run --rm \
 -v "$directory_path:/data" \
 novarx/pandoc \
    "*.md" \
    -o Semesterarbeit_JEE_Sterchi.pdf \
    -V fontsize=12pt \
    -V papersize=a4paper \
    --csl=https://raw.githubusercontent.com/citation-style-language/styles/master/din-1505-2-numeric-alphabetical.csl \
    --bibliography bib.bib \
    --pdf-engine=xelatex \
    --filter=pandoc-crossref \
    --filter=pandoc-plantuml \
    --filter=pandoc-svg
