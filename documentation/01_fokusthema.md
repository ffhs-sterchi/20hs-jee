# Fokusthema - Message Driven

## Grundbegriffe

### Messaging

Messaging ist eine Kommunikation-Art welche es erlaubt, Daten entkoppelt aus zu tauschen. Man kommuniziert also über eine definierte Schnittstelle in einem beliebigen Datenformat, welches idealerweise nicht abhängig von der verwendeten Sprache oder gar vom verwendeten System ist.

Die verschiedenen Systeme müssen sich also einig sein wie eine solche Nachricht auszusehen hat, wie diese Daten allerdings aggregiert oder verarbeitet werden, soll der Gegenpartei egal sein.

Heutzutage kommuniziert ein Grossteil der verteilten Systeme wohl über "Messages" wie z.B. Rest-Calls, welche im Grundsatz auch eine Nachricht darstellen. Daten werden zu einem wohldefinierten JSON serialisiert und können von beliebigen Konsumenten verwendet werden.

### Synchron vs. Asynchron

Viele Aufgaben, welche bearbeitet werden sollen müssen allerdings nicht immer gleich sofort ausgeführt werden oder aber eine Aktion könnte gleich mehrere Systeme gleichzeitig betreffen. Für solche Fälle sind synchrone Aufrufe meist eine schlechte Idee, wobei mit synchron gemeint ist, dass ein Client eine Anfrage stellt und darauf wartet, bis seine Aktion komplett beendet wurde.

Um zu umgehen, dass ein Client Sekunden- oder gar Minutenlang auf eine Antwort wartet, kann man eine asynchrone Kommunikation verwenden. Metaphorisch heisst das, der "Kunde" schildert uns sein Anliegen, wir nehmen es auf und geben es weiter an die zuständige Stelle.

Damit solche Arbeiten "weitergegeben" werden können bedarf es einer Art Warteschlange, also einer "Queue".

### Queue

Systeme welche Messages asynchron bearbeiten wollen, brauchen also einen Mechanismus die anstehenden Arbeiten zu ordnen und für die Weiterbearbeitung zu merken. Dabei spricht man i.d.R. von einer _Queue_, einem Mechanismus, welchem eine Nachricht übergeben werden kann und von einem oder mehreren Konsumenten meist chronologisch konsumiert werden kann.

Eine Queue kann man sich als Silo vorstellen, welches von oben mit neuen Daten befüllt wird und unten fortlaufend abtransportiert wird.

```plantuml
@startuml
skinparam ArrowColor DarkSlateGrey
skinparam agent {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
skinparam card {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
skinparam queue {
  fontColor white
  backgroundColor grey
  borderColor DarkSlateGrey
}

card Client
agent Producer
queue Queue

card Consumer1
card Consumer2
card Consumer3
card ConsumerN

Client -> Producer
Producer -> Queue
Consumer1 --o Queue
Queue o- Consumer2
Queue o-- Consumer3
Queue o-- ConsumerN
@enduml
```

### Message- oder Event-Driven

Wenn man von Message Driven spricht, meint man vielfach die technische Grundlage einer Event-Driven Architektur.

Bruns und Dunkel @bruns:dunkel definieren eine **Event-Driven** Architecture wie folgt:

> Event-Driven Architecture (EDA) als Architekturstil [...] stellen Ereignisse in das Zentrum der Softwareentwicklung und ermöglichen so ausdrucksmächtige Unternehmensanwendungen, die ein realitätsnahes Abbild der Wirklichkeit liefern.

Es geht also darum relevante Ereignisse publizieren und verarbeiten zu können. Und um das um zu setzen wird eben meist eine Art Queue benötigt um diese Ereignisse in ein Silo zu weiter Verarbeitung zu packen.

Das führt uns gleich zur Definition von **Message-Driven** von Dowalil @dowalil:

> [...]geht es beim Messaging um asynchrone Kommunikation. Man reduziert die Abhängigkeit zwischen Sender und Empfänger um die zeitliche Komponente. Das bedeutet, dass Sender und Empfänger nicht gleichzeitig online sein müssen, es wird also keine zeitliche Annahme getroffen. 
>
> Prinzipiell unterscheidet man dabei zwischen Message-Bus und Message-Broker. Während ein Bus agnostisch darüber ist, wer Nachrichten publiziert und wer welche konsumiert, ist ein Broker darüber hinaus für das Routing der Nachrichten zu den konkreten Empfängern zuständig.


Dabei gibt es ganz allgemein zwei grundsätzliche Patterns für das oben genannte Routing, welche dabei zur Anwendung kommen:

#### Command
\hfill\break

Dabei können viele Clients über einen Message-Bus den Wunsch kundtun, dass eine bestimmte Aktion ausgeführt werden soll. Man kann sich also wiederum einen Rest-Call vorstellen, welcher deponiert wird, die eigentliche Aktion allerdings erst beliebige Zeit später von einem "Worker" bearbeitet wird.

Bei der Architektur ist hier zu beachten, dass der Konsument, also in dem Fall derjenige der die Aktion ausführt (Worker), die Hoheit haben sollte, wie ein solches Event auszusehen hat.

#### Event
\hfill\break

Ein Event ist wie eben gerade definiert ein Ereignis, welches von einer oder mehreren logischen Quellen produziert wird für die "Allgemeinheit", wer auch immer der definierte Konsumentenkreis ist. Es gibt also im Gegensatz zum Command-Pattern in der Regel wenige Produzenten und viele Konsumenten. Wobei es den Produzenten grundsätzlich egal ist, was die Konsumenten mit den Daten machen, sie informieren lediglich darüber.

Hier wiederum sollte die architektonische Definition der Messages hingegen von den Produzenten bestimmt werden.


## Use-Cases Online-Shop

Um nun diesen Grundlagen eine Anwendung zu verleihen wird ein Konzept erarbeitet um Events im zu erstellenden Online-Shop zu verwalten. Zu beachten ist, dass es hier in erster Linie um das Konzept geht, implementiert wird wahrscheinlich nur ein Bruchteil davon im Rahmen dieses Projektes. Das effektiv umzusetzende Design befindet sich im nächsten Kapitel.

### Übersicht

Eine Komponenten-Übersicht der für die Events relevanten Komponenten könnte wie folgt aussehen:

```{.plantuml width=45%}
@startuml
skinparam shadowing false
skinparam ArrowColor DarkSlateGrey
skinparam agent {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
skinparam card {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
skinparam boundary {
  backgroundColor WhiteSmoke
  borderColor DarkSlateGrey
}
skinparam queue {
  fontColor white
  backgroundColor grey
  borderColor DarkSlateGrey
}

package Online-Shop {
    boundary OrderResource
    boundary ArticleResource
    queue ArticleViews
    queue Orders
    queue Inventory

    agent RecommendationService
    agent OrderService
}
OrderResource --> Orders #darkorange : Command
Orders o--- OrderService
Orders o-- RecommendationService

ArticleViews o-- RecommendationService
ArticleResource --> ArticleViews #darkorange : Event

ArticleResource --o Inventory
Inventory <--- OrderService #darkorange : Event\nSourcing
@enduml
```

Zu sehen sind darauf drei verschiedene Queues, `Inventory` für das Inventar, `ArticleViews` um sich angesehene Artikel zu merken und `Orders` für Bestellungen. Dabei haben diese Queues alle andere Aufgaben und werden mit anderen Patterns verwendet. 

### Artikel-Ansichten

Die Queue `ArticleViews` wird befüllt von der `ArticleResource` also dem Endpoint, welcher u.a. für das "ausliefern" der Artikeldaten ist.

Wann immer also ein Kunde einen Artikel ansieht, wird das in die Queue geschrieben. Dadurch kann der `RecommendationService` die generierten Daten auslesen und dazu verwenden den Kunden mitzuteilen, welche Artikel gerade von anderen Kunden angesehen werden. Es können aber künftig auch beliebig andere Konsumenten die Queue konsumieren um (spekulativ) z.B. ein Frühwarnsystem zu entwickeln, welches ein hohes Interesse an einem bestimmten Artikel meldet um frühzeitig eine Nachbestellung zu tätigen.

Hierbei handelt es sich um ein reines Event-Pattern.



### Bestellungen

Die Queue `Orders` nimmt Bestellungen welche über den Endpoint `OrderResource` eintreffen entgegen. Die eingetroffenen Commands werden dann vom `OrderService` gelesen und die Bestellungen entsprechend weiterverarbeitet. Hier bin ich der Meinung, dass es sich um eine Mischform der Event- und Command-Patterns handelt. Denn die Nachrichten welche eintreffen sind hauptsächlich dafür da, eine Bestellung auszulösen. Durch die Entkoppelung muss der Kunde nicht warten, bis ggf. aufwändige Arbeiten wie PDFs generieren oder ähnliches abgeschlossen sind.

Zusätzlich kann allerdings auch der `RecommendationService` auf getätigte Bestellungen zugreifen um den Kunden selbst oder anderen Kunden möglichst Zeitnahe Vorschläge zu unterbreiten, was sie sonst noch interessieren könnten.

### Inventar

Ein Inventar in einer Queue zu führen ist vielleicht etwas unorthodox, dennoch kann das sinvoll sein. Normalerweise denkt man bei einer Queue eher an eben das metaphorische Silo, welches einmal konsumiert wird und die Daten dann weg sind. Mit dem weiteren Pattern Event-Sourcing [^fowler], auf welches ich nicht genauer eingehen möchte, ist dies aber ohne weiteres möglich.





## Event Driven in Java

JEE bietet bereits einfache Mechanismen um mit Messages zu arbeiten. So ist z.B. der _Java Message Service_ (JMS) oder auch _Message Driven Beans_ (MDB) teil der JEE-Spezifikation. [^jee:mdb] [^jee:jms]

Der JMS bietet Queuing-Mechanismen an um Messages in eine Queue zu füllen, Message Driven Beans hingegen können Queues konsumieren.

### JEE Message Driven Beans

Ein MDB in JEE hat genau zwei Zustände:

- nicht Existierend im Container
- Erstellt und bereit Nachrichten zu empfangen

Sobald also ein MDB instanziiert wurde, wird diese fortlaufend Nachrichten aus der Queue konsumieren.

Beispiele, wie eine solches Bean erstellt werden kann finden sich haufenweise Online, hier eines von _baeldung.com_ [^baeldung]

```java
@MessageDriven(activationConfig = { 
	    @ActivationConfigProperty(
	      propertyName = "destination", 
	      propertyValue = "tutorialQueue"), 
	    @ActivationConfigProperty(
	      propertyName = "destinationType", 
	      propertyValue = "javax.jms.Queue")
	})
	public class ReadMessageMDB implements MessageListener {
	 
	    public void onMessage(Message message) {
	        TextMessage textMessage = (TextMessage) message;
	        try {
	            System.out.println("Message received: " + textMessage.getText());
	        } catch (JMSException e) {
	            System.out.println(
	              "Error while trying to consume messages: " + e.getMessage());
	        }
	    }
	}
```

<small>Da ich im Projekt nicht MDBs verwendet habe sondern das SmallRye [^SmallRye] Framework, habe ich kein selbst geschriebenes Beispiel</small>

### Framework - SmallRye Reactive Messaging

Für das Projekt habe ich das Framework _SmallRye Reactive Messaging_ verwendet, welches die Verwendung verschiedener Queues auf einfachste Weise implementiert.

Das Grundkonzept ist allerdings dasselbe wie bei den MDBs, sobald der Konsument instanziiert wurde, werden Nachrichten konsumiert.

Mit SmallRye ist das lediglich eine Annotation an einer Methode, welche die Daten verarbeiten soll. <small>(Natürlich nebst ein paar Konfigurationen, wo die Queue liegt)</small>

```java
@Incoming(INCOMING_CHANNEL_NAME)
public void consumeEvents(Event event) {
    if (event.path.startsWith("/article/")) {
        lastViewedArticle.onNext(event.path);
    }
}
```

Dieses Beispiel zeigt, wie eine bestimmte Queue konsumiert werden kann. Die Methode hat einen bestimmten Input-Parameter, welcher die konsumierte Nachricht darstellt welche verarbeitet werden soll.



## Patterns

Bei den Grundbegriffen haben wir zwei gängige Patterns für die Verteilung bzw. das Routing von Messages gesehen.
Dieses Routing ist insbesondere für Message-Driven-Beans massgebend, denn MDB kümmern sich ja in erster Linie um das Konsumieren von Messages.

Schaut man sich allerdings die gesamte Landschaft einer Message Driven Architektur an, gibt es noch weitaus mehr zu beachten.
Eine gute Übersicht des Themas bietet das Buch "Enterprise Integration Patterns" @hohpe:woolf von Hohpe & Woolf. Darin ist die Rede von 
6 verschiedenen Arten von Teil-Konstrukte einer Message Driven Architektur:

- Messaging Endpoints
- Message Construction
- Messaging Channels
- Message Routing
- Message Transformation
- Systems Management

Wobei _Systems Management_ nur bedingt mit Messaging als solches zu tun hat, dort geht es mehr um das Monitoring 
und Steuerung des Systems.

Für einige der genannten Konstrukte möchte ich _ein_ Pattern zusammenfassend aufführen.


### Correlation Identifier

**Message Construction**

Bei der Erstellung einer Message soll eine sogenannte Correlation ID mitgegeben werden. Eine solche ID identifiziert 
dann ein Ereignis eindeutig. Wichtiger Punkt dabei ist, dass diese ID auch bei z.B. weiterführenden Messages oder auch bei Antworten
mitgeführt wird. Durch diese ID kann z.B. eine Aktion verfolgt werden oder einzelne Business Prozesse Schritte zusammengefasst werden.


### Publish-Subscribe Channel

**Messaging Channels**

Wenn man von Message-Driven spricht, ist Pub/Sub wohl das geläufigste Pattern das man kennt.
Es handelt sich dabei um eines von vielen Channel-Patterns, dabei geht es also um die Verteilung von Messages.

Beim Publish-Subscribe Pattern geht es insb. darum, dass es eine bestimmte Anzahl Kanäle gibt, welche eindeutig identifizierbar sind.
Es gibt dann meist einen oder aber auch beliebig viele Publisher, welche in einen oder mehrere solche Kanäle eine Nachricht absetzt.

Diese Kanäle, bzw. die darin geschriebenen Nachrichten können schliesslich von einem oder meist vielen Subscriber abonniert werden.
Das Pattern ist technisch sehr nah verwandt mit dem Observer-Pattern[^observer-pattern]. So werden alle Abonnenten benachrichtigt, wenn eine neue Nachricht eingetroffen ist.



### Content-Enricher / -Filter

**Message Transformation**

Das _Message Transformation_ Konstrukt finde ich besonders interessant und ist ein wichtiger Teil wenn es um Message-Driven geht.

Bei einem Content-Filter geht es in erster Linie darum uninteressante Nachrichten zu Filtern. Da Messaging Systeme per se darauf
ausgelegt sind, mit einer Aktion viele Konsumenten zu bedienen, ist es umso wichtiger, dass sich Konsumenten auch nur an dem 
bedienen können, was sie brauchen.

Beim Content-Enricher geht es darum Nachrichten mit mehr Informationen anzureichern. So kann ein Konsument eine Nachricht 
lesen, mit seinem eigenen Wissen anreichern und weiter senden z.B. in einen anderen Message-Channel.





[^fowler]: https://martinfowler.com/eaaDev/EventSourcing.html
[^baeldung]: https://www.baeldung.com/ejb-message-driven-beans
[^SmallRye]: https://smallrye.io/smallrye-reactive-messaging/smallrye-reactive-messaging/2/index.html
[^jee:mdb]: https://docs.oracle.com/cd/E24329_01/web.1211/e24977/understanding.htm#WLMDB118
[^jee:jms]: https://docs.oracle.com/cd/B19306_01/server.102/b14257/jm_create.htm
[^observer-pattern]: https://en.wikipedia.org/wiki/Observer_pattern






