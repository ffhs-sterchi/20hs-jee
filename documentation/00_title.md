---
title: "Semesterarbeit JEE"
author: "D.Sterchi"
figureTitle: Abbildung #
lofTitle: ""
header-includes:
    - \usepackage{xcolor}
    - \usepackage{colortbl}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \fancyhead[L]{D.Sterchi}
    - \fancyhead[C]{BSc INF 2016.BE1}
    - \fancyhead[R]{HS20/21}
geometry: "left=3cm,right=3cm,top=3.5cm,bottom=2cm"
link-citations: true
numbersections: true
---

\pagenumbering{gobble}
\setcounter{tocdepth}{2}
\tableofcontents
\newpage

\pagenumbering{arabic}

# Einleitung
Es wird ein eigener Online Shop entwickelt, wobei das individuelle Fokus-Thema "_Message-Driven_" bearbeitet wird. Welches in Absprache mit dem Dozenten gewählt wurde.

Der Onlineshop muss insgesamt funktionsfähig und dokumentiert sein.
Das Hauptgewicht der Semesterarbeit liegt auf der theoretischen Behandlung, der Analyse, dem Design, der Implementation und dem Test des ausgewählten Fokus-Themas.

Der Onlineshop soll insb. folgende Funktionalitäten haben:

- Verwaltung von Kunden
- Registrierung / Deregisitrierung von Kunden
- Anpassung der Eigenschaften von Kunden 
- Verwaltung der Einkäufe und Verkäufe pro Kunde
- Warenkorbverwaltung pro Kunde
- Verwaltung von angebotenen Produkte pro Kunde:
  -Hinzufügen und Entfernen von Produkten
  -Anpassung von Produkten (Anzahl im Lager, Name, ID, usw.)
