# Online-Shop

📝 [**Dokument**](https://gitlab.com/ffhs-sterchi/20hs-jee/builds/artifacts/master/raw/documentation/out/Semesterarbeit_JEE_Sterchi.pdf?job=document&inline=false) als PDF
&nbsp;|&nbsp;
📺 [**Präsentation**](https://ffhs-sterchi.gitlab.io/20hs-jee/slides/) online
&nbsp;|&nbsp;
📈 [**Präsentation**](https://ffhs-sterchi.gitlab.io/20hs-jee/slides/Semesterarbeit_JEE_Sterchi_Presentation.pdf) als PDF
&nbsp;|&nbsp;
✅ [**Test-Coverage**](https://ffhs-sterchi.gitlab.io/20hs-jee/coverage)

## Lokal starten

```shell script
docker-compose pull && docker-compose up
```
<small>([Docker](https://docs.docker.com/get-docker/) muss auf dem System vorhanden sein)</small>

Um den Webshop lokal zu starten kann man im Root-Verzeichnis des Projektes den obenstehenden Befehl ausführen und kurz warten bis die Container laufen (wenn die Konsole nicht mehr permanent Zeilen loggt).


### URLs

- Das Shop-Frontend ist erreichbar unter: **http://localhost:4200/**
- Ein Swagger-UI für das Backend ist erreichbar unter: **http://localhost:8080/swagger-ui/**
- Die Testabdeckung kann unter **https://ffhs-sterchi.gitlab.io/20hs-jee/coverage** eingesehen werden.


### E2E-Tests
Es gibt im Frontend E2E-Tests welche die Integration und ein minimales Verhalten testen.

Möchte man diese ausführen, muss das komplette System am laufen sein, also z.B. via dem oben stehenden `docker-compose up`.

Ebenfalls muss im Verzeichnis _./client_ der Befehl `npm i` ausgeführt werden um die Dependencies zu laden. Schliesslich können die Tests mit einem der folgenden Befehle gestartet werden.

```shell script
# Interaktive Tests (es öffnet sich ein Browser)
npm run cy:start

# Tests einmal in der Command-Line (headless) ausführen
npm run cy:ci
```
